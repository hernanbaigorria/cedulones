<?php

//require_once("../scripts/functions.php");
// Including all required classes for barcode
require_once('barcode/BCGFontFile.php');
require_once('barcode/BCGColor.php');
require_once('barcode/BCGDrawing.php');

// Including the barcode technology
require_once('barcode/BCGi25.barcode.php');

function simple_encrypt($text) {
    return trim(base64_encode($text));
}

function simple_decrypt($text) {
    return trim(base64_decode($text));
}

function generateBarcode() {

    $get = trim(base64_encode('cod'));
    $gCode = trim(base64_decode($_GET[$get]));

    // Loading Font
    $font = new BCGFontFile('barcode/font/Arial.ttf', 18);

    // The arguments are R, G, B for color.
    $color_black = new BCGColor(0, 0, 0);
    $color_white = new BCGColor(255, 255, 255);

    $drawException = null;
    try {
        $code = new BCGi25();
        $code->setScale(2); // Resolution
        $code->setThickness(25); // Thickness
        $code->setForegroundColor($color_black); // Color of bars
        $code->setBackgroundColor($color_white); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse($gCode); // Text
    } catch (Exception $exception) {
        $drawException = $exception;
    }

    /* Here is the list of the arguments
      1 - Filename (empty : display on screen)
      2 - Background color */
    $drawing = new BCGDrawing('', $color_white);
    if ($drawException) {
        $drawing->drawException($drawException);
    } else {
        $drawing->setBarcode($code);
        $drawing->draw();
    }

// Header that says it is an image (remove it if you save the barcode to a file)
    header('Content-Type: image/png');
    header('Content-Disposition: inline; filename="barcode.png"');
// Draw (or save) the image into PNG format.
    $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
}

echo generateBarcode();

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deudas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       date_default_timezone_set('America/Argentina/Buenos_Aires');
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   
	   $this->load->library('session');
	   $this->load->library('ppt');
	} 
	 
	
	public function index()
	{	

		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
		//$this->template->add_css('asset/css/home.css');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Cedulones - Home', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		$CI =& get_instance();	
		
		$this->template->write_view('content', 'layout/deudas/main', $data);
		$this->template->write_view('header', 'layout/header', $data);
		$this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	}


	public function detalle(){
		delete_cookie('selecciondos');
		delete_cookie('numdeuda');
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
		//$this->template->add_css('asset/css/home.css');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Cedulones - Home', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		$CI =& get_instance();

		$data['tableheader'] = $this->page_model->get_tableheader();
		 // Obtengo el id de la URL con Cookies generadas anteriormente
		 $id = $this->input->cookie('identi',false);
		 $type = $this->input->cookie('type',false);

		 // La variable $i solo suma dentro del while
		 // Luego la utilizo para poner delante de los datos de montos de la tabla que se genera 
		 // Para así poder diferenciarlos. Eso es particularmente por el diseño doble de mobile y pantallas grandes
		 $i = 0;

		 // expire_year_month = Año y mes de vencimiento para luego poder comparar contra la tabla fechasinteres
		 // expire_day = Día de vencimiento para calcular el interes diario del mes de vencimiento
		 // debt = La deuda a la fecha de vencimiento
		 $aRegExpire = $this->page_model->get_deudas($id,$type);


		 foreach ($aRegExpire as $fila) {
		 $expireYearMonth = $fila->expire_year.$fila->expire_month;

		//print_r($expireYearMonth);
		 $expireDay = $fila->expire_day;


		 
		 $expireComplete = date('d/m/Y',strtotime($fila->expire_complete));
		 $expireCompleteTraditional = date('Y/m/d',strtotime($fila->expire_complete_traditional));
		 $cNumber = $fila->c_number;
		 $aNumber = $fila->a_number;
		 $tName = $fila->t_name;
		 $stateName = $fila->state_name;
		 $situation = $fila->situation;
		 $situationRate = $fila->situation_rate;
		 $debtAmount = $fila->debt_amount;


		date_default_timezone_set('America/Argentina/Buenos_Aires');

	    $interes = $this->page_model->get_interes($type, $expireYearMonth);
		
		$rateTotalT = 0;

		$i++;

			if($interes != NULL):
		 	// Recorro los datos devueltos de la consulta
		 	foreach($interes as $key => $value){

		 	    // $rate = Interes
		 	    // $iexpireYearMonth = Año|Mes fechasinteres
		 	    
		 	    $rate = $interes[$key]['rate'];
		 		
		 	    $iexpireYearMonth = $interes[$key]['iexpire_year_month'];
		 	   	//var_dump('fecha: '.$int->iexpire_year_month);
		 	    // Mediante la funcion getMonthAmountDays y el parametro 'returnDaysPlus' devuelvo la cantidad de días que deseo adiccionar como intereses diarios
		 	    // Al valor final total
		 	    $daysPlus = $this->page_model->getMonthAmountDays($iexpireYearMonth, 'returnDaysPlus');

		 	    $expireCompare = strtotime($expireCompleteTraditional);
		 	    $todayPlus7 = strtotime(date("Y/m/d") . "+ $daysPlus days");

		 	    if ($expireCompare <= $todayPlus7) {
		 	        // Por defecto $rateCalc tiene el valor devuelto de la consulta
		 	        $rateCalc = $rate;
		 	        // Mediante la funcion getMonthAmountDays devuelvo la cantidad de días que tiene un X mes, un X año
		 	        $days = $this->page_model->getMonthAmountDays($iexpireYearMonth);

		 	        // Si el año|mes de fechasinteres coincide con el año|mes de vencimiento
		 	        // O si el año|mes de fechasinteres coincide con el año|mes corriente
		 	        // Calculo la cantidad de días restantes del mes para así poder determinar el interes diario
		 	        $iexpireYearMonthF = strtotime(date("Ymd", strtotime($iexpireYearMonth . "01")));
		 	        $expireYearMonthF = substr(str_replace("/", "", $expireCompleteTraditional), 0, 6) . "01";
		 	        $expireYearMonthF = strtotime(date("Ymd", strtotime($expireYearMonthF)));

		 	        if ($iexpireYearMonthF == $expireYearMonthF || $iexpireYearMonth == DATE('Ym')) {
		 	            // Si el año|mes de fechainteres es igual al año|mes actual
		 	            if ($iexpireYearMonth == DATE('Ym')) {
		 	                // Calculo el interes diario del prinicipio del mes actual al día de la fecha
		 	                $rateCalc = ((DATE('d') * $rate) / $days);
		 	            } else {
		 	                // En caso que el año|mes de fechasinteres coincide con el año|mes de vencimiento
		 	                // Le resto al dia de vencimiento el total de días del mes de vencimiento para así calcular el interes diario
		 	                $rateCalc = ((($days - $expireDay) * $rate) / $days);
		 	            }
		 	        }
		 	        // Hago una segunda validación y solo en el caso que sea el último mes, por ende el mes actual, 
		 	        // Le sumo X días de interéses determinados en la funcion getMonthAmountDays con el parámetro 'returnDaysPlus'
		 	        $ratePlus = ($daysPlus * $rate) / $days;
		 	        $rateCalc = $rateCalc + $ratePlus;
		 	    } else {
		 	        $rateCalc = '0';
		 	    }

		 	    //var_dump($rateTotalT);

		 	    // Cálculo del valor de intereses por cada deuda
		 	    $rateTotal = (($debtAmount * $rateCalc) / 100);

		 	    // Suma total de los valores de intereses
		 	    $rateTotalT = $rateTotalT + $rateTotal;

		 	    //var_dump($iexpireYearMonth);
		 	    //break;


		 	}
		 	endif;

		 	// Suma del valor total de intereses más la deuda, más los X días que se agregan, más los honorarios. Corto en 2 decimales. Doy formato de moneda
		 	$tDebtRateDebtPlus = bcdiv(($rateTotalT + $debtAmount + $this->page_model->calcFee($situation, $situationRate, $rateTotalT + $debtAmount)), 1, 2);

		 	$honorarios = bcdiv($this->page_model->calcFee($situation, $situationRate, $rateTotalT + $debtAmount),1,2);
		 	$recargos = bcdiv($rateTotalT, 1, 2);

		 	// En caso que no necesite devolver el total, construyo la tabla
		 	if ($return != "total") {
		 	    $data['tabledeudas'] .= $this->page_model->rInmHtmlConstruc($tName, $aNumber, $expireComplete, $debtAmount,$recargos,$honorarios, $tDebtRateDebtPlus, $stateName, $cNumber,false, false, $i);
		 	}
		 	// Sumo todos los totales de cada cuota
		 	$debtTotal = $debtTotal + $tDebtRateDebtPlus;

		 }

		 $aPLanes = $this->page_model->get_zvistaplanes($id);

		 foreach ($aPLanes as $fila) {
		 $expireYearMonth = $fila->expire_year.$fila->expire_month;

		//print_r($expireYearMonth);
		 $expireDay = $fila->expire_day;
		 $expireComplete = date('d/m/Y',strtotime($fila->expire_complete));
		 $expireCompleteTraditional = date('Y/m/d',strtotime($fila->expire_complete_traditional));
		 $cNumber = $fila->c_number;
		 $aNumber = $fila->a_number;
		 $tName = $fila->t_name;
		 $tCuotas = $fila->t_cuotas;
		 $pCuotas = $fila->p_cuotas;
		 $stateName = $fila->state_name;
		 $situation = $fila->situation;
		 $situationRate = $fila->situation_rate;
		 $debtAmount = $fila->debt_amount;


		 date_default_timezone_set('America/Argentina/Buenos_Aires');


		$interes = $this->page_model->get_interes($type, $expireYearMonth);

		$rateTotalT = 0;

		$i++;


		 	// Recorro los datos devueltos de la consulta
		 	foreach($interes as $key => $value){

		 		
		 	    // $rate = Interes
		 	    // $iexpireYearMonth = Año|Mes fechasinteres
		 	    $rate = $interes[$key]['rate'];
		 	    $iexpireYearMonth = $interes[$key]['iexpire_year_month'];
		 	   	//var_dump('fecha: '.$int->iexpire_year_month);
		 	    // Mediante la funcion getMonthAmountDays y el parametro 'returnDaysPlus' devuelvo la cantidad de días que deseo adiccionar como intereses diarios
		 	    // Al valor final total
		 	    $daysPlus = $this->page_model->getMonthAmountDays($iexpireYearMonth, 'returnDaysPlus');


		 	    $expireCompare = strtotime($expireCompleteTraditional);
		 	    $todayPlus7 = strtotime(date("Y/m/d") . "+ $daysPlus days");

		 	    if ($expireCompare <= $todayPlus7) {
		 	        // Por defecto $rateCalc tiene el valor devuelto de la consulta
		 	        $rateCalc = $rate;
		 	        // Mediante la funcion getMonthAmountDays devuelvo la cantidad de días que tiene un X mes, un X año
		 	        $days = $this->page_model->getMonthAmountDays($iexpireYearMonth);

		 	        // Si el año|mes de fechasinteres coincide con el año|mes de vencimiento
		 	        // O si el año|mes de fechasinteres coincide con el año|mes corriente
		 	        // Calculo la cantidad de días restantes del mes para así poder determinar el interes diario
		 	        $iexpireYearMonthF = strtotime(date("Ymd", strtotime($iexpireYearMonth . "01")));
		 	        $expireYearMonthF = substr(str_replace("/", "", $expireCompleteTraditional), 0, 6) . "01";
		 	        $expireYearMonthF = strtotime(date("Ymd", strtotime($expireYearMonthF)));

		 	        if ($iexpireYearMonthF == $expireYearMonthF || $iexpireYearMonth == DATE('Ym')) {
		 	            // Si el año|mes de fechainteres es igual al año|mes actual
		 	            if ($iexpireYearMonth == DATE('Ym')) {
		 	                // Calculo el interes diario del prinicipio del mes actual al día de la fecha
		 	                $rateCalc = ((DATE('d') * $rate) / $days);
		 	            } else {
		 	                // En caso que el año|mes de fechasinteres coincide con el año|mes de vencimiento
		 	                // Le resto al dia de vencimiento el total de días del mes de vencimiento para así calcular el interes diario
		 	                $rateCalc = ((($days - $expireDay) * $rate) / $days);
		 	            }
		 	        }
		 	        // Hago una segunda validación y solo en el caso que sea el último mes, por ende el mes actual, 
		 	        // Le sumo X días de interéses determinados en la funcion getMonthAmountDays con el parámetro 'returnDaysPlus'
		 	        $ratePlus = ($daysPlus * $rate) / $days;
		 	        $rateCalc = $rateCalc + $ratePlus;
		 	    } else {
		 	        $rateCalc = '0';
		 	    }

		 	    //var_dump($rateTotalT);

		 	    // Cálculo del valor de intereses por cada deuda
		 	    $rateTotal = (($debtAmount * $rateCalc) / 100);

		 	    // Suma total de los valores de intereses
		 	    $rateTotalT = $rateTotalT + $rateTotal;

		 	    //var_dump($iexpireYearMonth);
		 	    //break;


		 	}

		 	// Suma del valor total de intereses más la deuda, más los X días que se agregan, más los honorarios. Corto en 2 decimales. Doy formato de moneda
		 	$tDebtRateDebtPlus = bcdiv(($rateTotalT + $debtAmount + $this->page_model->calcFee($situation, $situationRate, $rateTotalT + $debtAmount)), 1, 2);

		 	$honorarios = bcdiv($this->page_model->calcFee($situation, $situationRate, $rateTotalT + $debtAmount),1,2);
		 	$recargos = bcdiv($rateTotalT, 1, 2);

		 	// En caso que no necesite devolver el total, construyo la tabla
		 	if ($return != "total") {
		 	    $data['tableplanes'] .= $this->page_model->rInmHtmlConstruc($tName, $aNumber, $expireComplete, $debtAmount,$recargos,$honorarios, $tDebtRateDebtPlus, $stateName, $cNumber,$tCuotas, $pCuotas, $i);
		 	}
		 	// Sumo todos los totales de cada cuota

		 }

		 $data['totaldeuda'] = $debtTotal;
		
		$this->template->write_view('content', 'layout/deudas/detalle', $data);
		$this->template->write_view('header', 'layout/header', $data);
		$this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();

	}

	/* FUNCION PARA GENERAR EL PDF */
	public function send()
	{	

		
	   $id = $this->input->cookie('identi',false);
	   $type = $this->input->cookie('type',false);

       $selecciondos = json_decode($_COOKIE['checkbox'], false);
       $ndeuda = json_decode($_COOKIE['numdeuda'], false);


		if($_POST['accion'] == 'pdf'){

			$this->page_model->exportarPDF($id, $selecciondos, $ndeuda);
		}

	}

	/* FUNCION PARA GENERAR EL PAGO ONLINE */
	function pago() {
		date_default_timezone_set('America/Argentina/Buenos_Aires');
	    $user = "m6ben5j9d9s4q8gg";
	    $pass = "0q2dZa0pCK6WodR7ZE";
	    $cliId = "16465308-1844-4abe-abe6-f184149ee740";
	    $cliSec = "a2d03fa3-f6c4-45e5-9792-dc0d8b51a25c";
	    // Identificación del Bien
	    $id = $this->input->cookie('identi',false);
	    // Nombre del titular
	    $titulo = $this->page_model->get_registro_id($id);

	    $tit = ucwords($titulo[0]->resp);
	    $name = $tit;
	    // Numero de recibo generado
	    $receipt = $this->page_model->getNextReceiptNum(TRUE);
	    // Obtengo la data
	    $details = array();
	    $datas = explode("-", substr($_POST["data"], 0, -1));

	    
	    foreach ($datas as $d) {
	        $d = explode("_", $d);
	        $anpe = $d[0];
	        $amount = $d[1];
	        $description = "$id $anpe";
	        $details[] = array(
	            'external_reference' => $receipt,
	            'concept_id' => $receipt,
	            'concept_description' => $description,
	            'amount' => $amount,
	        );
	    }

	    $data = array(
	        "notification_url" => $GLOBALS['fullUrl'] . "/ppt_payr.php",
	        "collector_id" => "1697",
	        'currency_id' => 'ARS',
	        'external_transaction_id' => date_timestamp_get(date_create()),
	        'details' => $details,
	        'payer' => array(
	            'name' => $name,
	            'email' => '',
	            'identification' => array(
	                'type' => 'DNI_ARG',
	                'number' => '11222333',
	                'country' => 'ARG'
	            )
	        ),
	        'metadata' => array(
	            'valor1' => '123'
	        ),
	    );

	    // Inserto los registros de recibos
	    $this->page_model->receiptInsert($receipt, $this->page_model->expDates('1'), $this->page_model->expDates('2'), $this->page_model->expDates('3'), $this->page_model->get_table_generator('total1'), $this->page_model->get_table_generator('total2'), $this->page_model->get_table_generator('total3'));
	    // Le paso como valor ppt por que no está contemplado en la función. Al ser así no me muestra ningún resultado, solo realiza la inserción de datos
	    $this->page_model->get_table_generator("ppt", "", "", $receipt, true);

	    $ppt = new PPT('1','2','3','5','7','9');

	    $result = $ppt->create_preference($data);
	    echo $result["response"]["form_url"];
	}

	/* GENERACION DE SESSION COOKIE */
	public function rdatacheck()
	{

		$id = str_replace("-", "", $_POST["identi"]);
		$type = $_POST["type"];

		/* Busco el BIEN_numero para encontrar sus deudas */
		$bienes = $this->page_model->get_bien_numero($id, $type);

		/* Busco si el Bien tiene deudas o no */
		$deuda = $this->page_model->get_deduda_existente($id, $type);


		/* SESSION COOKIE Del Bien y del Tipo */
		$identi= array(

		   'name'   => 'identi',
           'value'  => $id,                            
           'expire' => '999999',                                                                                   
           'secure' => false

       );

       $this->input->set_cookie($identi);

       $typename= array(

		   'name'   => 'type',
           'value'  => $type,                            
           'expire' => '999999',                                                                                   
           'secure' => false

       );

       $this->input->set_cookie($typename);

		if ($bienes <= 0) {
		    // No se encuenta el bien
		    echo "NB";
		} else if ($deuda <= 0) {
		    // No se encuenta ninguna deuda
		    echo "ND";
		}

	}

	/* FUNCION QUE BORRA EL PDF GURADADO EN EL SISTEMA UNA VEZ DESCARGADO */
	public function borrar()
	{	

		unlink('uploads/'.$_POST['file']);
		exit;

	}
	
	
}

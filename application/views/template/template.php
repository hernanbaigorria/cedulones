<!DOCTYPE html>
<html>
   <head>
   	  <meta charset="utf-8" />
      <title><?= $title ?></title>
	  <meta name="description" content="<?= $description ?>">
	  <meta name="keywords" content="<?= $keywords ?>">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
	  <meta property="og:title" content="<?= $title ?>" />
	  <meta property="og:type" content="<?= $ogType ?>" />
	  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
	  <meta property="og:image" content="<?= base_url().$image ?>" />
	  <meta property="og:description" content="<?= $description ?>" />
	  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>asset/img/favicon.ico">  

	  <!--external css-->
	  <link href="<?php echo base_url() ?>asset/css/font-awesome.5.6.3/all.css" rel="stylesheet" />  

	  <!--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->      
	  <link href="<?php echo base_url() ?>asset/css/metro.css?version=<?=time()?>" rel="stylesheet" type="text/css" />      
	  <link href="<?php echo base_url() ?>asset/css/app.css?version=<?=time()?>" rel="stylesheet" type="text/css" />

	  <!-- Custom styles for this template -->
	  <link href="<?php echo base_url() ?>asset/css/styles.css?version=<?=time()?>" rel="stylesheet">

	  <!-- CSS STYLES -->
	  <!-- Bootstrap core CSS -->
	  <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet">
	  <link href="<?php echo base_url() ?>asset/css/bootstrap-reboot.min.css" rel="stylesheet">

	  <script>var base_url = '<?php echo base_url() ?>';</script>

	  <?= $_styles ?>
	 

   </head>
   <body class="m-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">
   	<!-- begin::Page loader -->
   	<div class="m-page-loader m-page-loader--base">
   	    <div class="sk-folding-cube">
   	        <div class="sk-cube1 sk-cube"></div>
   	        <div class="sk-cube2 sk-cube"></div>
   	        <div class="sk-cube4 sk-cube"></div>
   	        <div class="sk-cube3 sk-cube"></div>
   	    </div>
   	    <h1 class="ml12 hidden">Espere por favor, se está generando el pago</h1>
   	</div>
   	<!-- end::Page Loader -->

   	<!-- begin:: Page -->
   	<div class="m-grid m-grid--hor m-grid--root m-page">



   		<!-- begin::Body -->
   		<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
   		    <div class="m-grid__item m-grid__item--fluid m-wrapper">
   		        <!-- END: Subheader -->
   		        <div class="m-content">
				<?= $header ?>        
				<?= $content ?>
			   	<?= $footer ?>
   	            <!--End::Section-->
   	        	</div>
   	    	</div>
   		</div>

   	</div>


	  <?= $_scripts ?>

	  
	  <script src="<?php echo base_url() ?>asset/js/jquery-3.3.1.min.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/jquery-ui.min.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/jquery.cookie.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/header.anim.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/anime.min.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/bootbox.min.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/cleave.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/webfont.js" type="text/javascript"></script>
	  <script src="<?php echo base_url() ?>asset/js/general.js?version=<?=time()?>" type="text/javascript"></script>
	 <!-- begin::Page Loader -->
	 <script>
	     $(window).on('load', function () {
	         $('body').removeClass('m-page--loading');
	     });
	     WebFont.load({
	         google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700", "Asap+Condensed:500"]},
	         active: function () {
	             sessionStorage.fonts = true;
	         }
	     });
	 </script>
    <script type="text/javascript">

	$(".send-proforma").submit(function(event){
		event.preventDefault(); //prevent default action 
  		var post_url = $(this).attr("action"); //get form action url
  		var request_method = $(this).attr("method"); //get form GET/POST method
  		var form_data = $(this).serialize(); //Encode form elements for submission
		$.ajax({
			url : post_url,
  			type: request_method,
  			data : form_data,
			beforeSend: function() {
		        // setting a timeout
		        $('.content-loding').show();
		    },
		    success: function(response){
		    		if(response == 'exito'){
		    			alert("Proforma enviada con exito");
		    		} else {
		    		  window.location.replace(response);
		    		}
		        $('.content-loding').hide();
		    }
		});
	});

	$('#btnChange').click(function(){
	    var textChange = $('#inputChange').val();
	    setCookie("textChange", textChange);
	    $('#refe').text(textChange);
	    $('#exampleModal').modal('hide');
	});

    </script>
   </body>
</html>
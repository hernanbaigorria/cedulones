<div class="row">
    <div class="col-xl-12">
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption w100perc">
                    <div class="m-portlet__head-title w100perc">
                        <a href="<?php echo base_url() ?>" class="col-2">
                            <i class="icon-fas-arrow-back style-icon-fas-col fas fa-arrow-circle-left"></i>
                        </a>
                        <div class="col-12">
                            <h3 class="m-portlet__head-text w100perc dis-block text-center fz23rem">
                                Impresión y pago online de deudas
                            </h3>
                        </div>
    <!--                    <div class="col-2">
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 cursor-p bootid" istype="inm">
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
            <div class="m-portlet__body dis-block text-center style-icon-fas-col">
                <div> 
                    <i class="icon-fas fas fa-home"></i>
                    <p class="icon-fas-text">Propiedad</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 cursor-p bootid" istype="rod">
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
            <div class="m-portlet__body dis-block text-center style-icon-fas-col">
                <div> 
                    <i class="icon-fas fas fa-car"></i>
                    <p class="icon-fas-text">Automotor</p>
                </div> 
            </div>
        </div>   
    </div>
    <div class="col-xl-3 cursor-p bootid" istype="com">
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
            <div class="m-portlet__body dis-block text-center style-icon-fas-col">
                <div> 
                    <i class="icon-fas fas fa-industry"></i>
                    <p class="icon-fas-text">Comercio e Industria</p>
                </div> 
            </div>
        </div>
    </div>
    <div class="col-xl-3 cursor-p bootid" istype="cem">
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
            <div class="m-portlet__body dis-block text-center style-icon-fas-col">
                <div>  
                    <i class="icon-fas fas fa-cross"></i>
                    <p class="icon-fas-text">Cementerio</p>
                </div> 
            </div>
        </div>   
    </div>
</div>

<!-- MODALES -->
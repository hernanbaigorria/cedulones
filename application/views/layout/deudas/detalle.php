<div class="row">
    <div class="col-12">
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption w100perc">
                    <div class="m-portlet__head-title w100perc">
                        <div class="col-2">
                            <i class="icon-fas-arrow-back style-icon-fas-col fas fa-arrow-circle-left"></i>
                        </div>
                        <div class="col-12">
                            <h3 class="m-portlet__head-text w100perc dis-block text-center fz23rem">
                                Impresión y pago online de deudas
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-1 hidden-sm hidden-xs">
    </div>
    <?php echo $tableheader ?>
    <div class="col-1 hidden-sm hidden-xs">
    </div>
</div>
<div class="row">
    <div class="col-3 hidden-sm hidden-xs"></div>
    <div class="col-lg-6 col-sm-12 col-xs-12">
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded">
            <div class="m-portlet__head h42px">
                <div class="m-portlet__head-caption w100perc h42px">
                    <div class="m-portlet__head-title w100perc">
                        <div class="col-12">
                            <h5 class="m-portlet__head-text w100perc dis-block text-center fz15rem">
                                Detalle de deudas de tasas, contribuciones e impuestos
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3 hidden-sm hidden-xs"></div>
</div>
<div class="row">
    <div class="col-1 hidden-sm hidden-xs">
    </div>
    <div class="col-lg-10 col-sm-12 col-xs-12">
        <table class="table table-bordered table_pay_detail">
            <thead>
                <tr class="hidden-sm hidden-xs">
                    
                    <th colspan="1" scope="col" class="brnone">
                        Elija el vencimiento
                    </th>
                    <th colspan="1" scope="col" class="blnone brnone">
                        <div class="wrapper">
                            <label for="datepicker ">
                                <input type="text" id="datepicker" class="selDaysPlus" autocomplete="off" style="font-size:15px;">
                            </label>    
                        </div>
                        <?php if(false): ?>
                            <select class='form-control selDaysPlus'>
                                <?php //$this->page_model->daysSelect(7); ?>
                            </select>
                        <?php endif; ?>
                    </th>
                    <th colspan="6" style="background:#fff;text-align:right;">
                        <span style="color:#000;text-shadow:none;cursor:pointer;"  id="refe" data-toggle="modal" data-target="#exampleModal">
                            Referencia
                        </span>
                    </th>
                </tr>
                <tr class="hidden-sm hidden-xs">
                    <th scope="col">Cuota</th>
                    <th scope="col">Vencimiento</th>
                    <th scope="col" style="opacity:.8;">Monto Original</th>
                    <th scope="col" style="opacity:.8;">Recargos</th>
                    <th scope="col" style="opacity:.8;">Honorarios<br>/ Gastos</th>
                    <th scope="col">Monto actualizado</th>
                    <th scope="col">Situación</th>
                    <th scope="col">
                        <?php if(false): ?>
                            <input name='checkbox1' all-on="0" check-box-suffix="3" class='styled payDetail' type='checkbox'>
                        <?php endif; ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <form class="tablaDeudas" method="post" action="<?php echo base_url() ?>deudas/send/">
                    <tr class='hidden-lg hidden-md'>
                        <td colspan="3" class=''>
                            <span class=pull-left>Días | Calcular pago</span>
                            <select class='form-control selDaysPlus'>
                                <?php //daysSelect(7); ?>
                            </select>
                        </td>
                    </tr>
                    <tr class='hidden-lg hidden-md'>
                        <td colspan="3" class=''>
                            <span class="table-header-xs-sm">Seleccionar todo</span>
                            <input name='checkboxXs' all-on='0' check-box-suffix="4" class='styled payDetail pull-right' type='checkbox' />
                        </td>
                    </tr>
                    <tr class='hidden-lg hidden-md'>
                        <td colspan='3' class='td-separator'></td>
                    </tr>
                    <?php echo $tabledeudas ?>
                    <?php echo $tableplanes ?>
                    <tr class='hidden-sm hidden-xs'>
                        <td colspan="8" class='table_pay_bottom_color text-center'>IMPORTE ADEUDADO <span style="margin:0 5% 0 2%">$ <?php echo $totaldeuda; ?></span> <span>S.E.U.O</span></td>
                    </tr>
                </form>
            </tbody>
        </table>
    </div>
    <div class="col-1 hidden-sm hidden-xs">
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="display:flex;align-items: ">
        <h5 class="modal-title" id="exampleModalLabel">Referencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left:auto;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p style="font-size: 14px;">
            <?php 
            $type = $this->input->cookie('type',false);
            if($type == 1): ?>
                <strong>TSP</strong> = Taza Servicio a la Propiedad <br>
            <?php endif; ?>
            <?php if($type == 2): ?>
                <strong>IAU</strong> = Impuesto al Automotor <br>
            <?php endif; ?>
            <?php if($type == 3): ?>
                <strong>TCI</strong> = Taza de Comercio de Industria <br>
            <?php endif; ?>
            <?php if($type == 4): ?>
                <strong>TC</strong> = Taza de Cementerio<br>
            <?php endif; ?>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="table_pay_bottom_fixed_lg">
    <div class="row">
        <div class="content-botones d-flex align-items-center justify-content-center">
            <button type="button" class="btn btn btn btn-primary disabled btn-cedu">Imprimir cedulón</button>
            <button type="button" class="btn btn btn btn-success disabled btn-pay-on">Pagar online</button>
            <div class='border-none table_pay_bottom_color text-center'>Importe seleccionado: $ <span class="detailTotal">0.00</span>
                    <input id="inputDetailTotal" value="0.00" class="hidden"/>
                    <input id="inputDetailEach" class="hidden" style="color: #000"/>
                    <input id="inputForPPT" class="hidden" style="color: #000"/>
                    <input id="inputTypeOf" value="<?php //echo //getTypeNum(); ?>" class="hidden" style="color: #000"/>
                    <input id="cNumbers" value="" class="hidden" style="color: #000"/>
            </div>
        </div>
        <?php  if(false): ?>
        <!--<div class="col-3"></div>-->
        <div class="table_pay_bottom_color maxw300">
            <table class="table table-bordered table_pay_detail border-none">
                <tbody>
                    <tr class='border-none'>
                        <td colspan="2" class='border-none table_pay_bottom_color text-center'>Importe seleccionado: $ <span class="detailTotal">0.00</span></td>
                        <!--<td colspan="2" class='border-none table_pay_bottom_color text-center'></td>-->
                    </tr>
                    <tr class='border-none'>
                        <td class='border-none table_pay_bottom_color text-right minw285 w285'>
                            <input id="inputDetailTotal" value="0.00" class="hidden"/>
                            <input id="inputDetailEach" class="hidden" style="color: #000"/>
                            <input id="inputForPPT" class="hidden" style="color: #000"/>
                            <input id="inputTypeOf" value="<?php //echo //getTypeNum(); ?>" class="hidden" style="color: #000"/>
                            <input id="cNumbers" value="" class="hidden" style="color: #000"/>
                            <button type="button" class="btn btn btn btn-primary disabled btn-cedu">Imprimir cedulón</button>
                            <button type="button" class="btn btn btn btn-success disabled btn-pay-on">Pagar online</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--<div class="col-3"></div>-->
        <?php endif; ?>
    </div>
</div>
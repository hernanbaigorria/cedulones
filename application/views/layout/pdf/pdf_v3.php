<style type="text/css">
    @page {
        size: auto;
        margin-top: 1cm;
        margin-bottom: 1cm;
        odd-header-name: MyHeader1;
        odd-footer-name: MyFooter1;
    }
</style>
<div class="container-fluid">
        <div class="row" style="border: 2px solid #000;padding: 5px 2px;width:100%margin: 0 auto;"> 
        	<table class="table table-mini-2 text-center" style="">
                <tr>
                	<td style="text-align:left;">
                		<img src="<?php echo base_url() ?>asset/img/logo_muni.png" style="width: 80px"/>

                	<td width="400" style="text-align:left;font-weight: bold">
                		<?php echo $municipio[0]->MUNI_nombre ?><br/>
                        <div style="font-size: 9px;">
                        <?php echo $municipio[0]->MUNI_calle ?><br/>
                        CP.:<?php echo $municipio[0]->MUNI_codpos ?> <?php echo $municipio[0]->LOCA_nombre ?> - <?php echo $municipio[0]->PCIA_nombre ?><br/>
                        <?php echo $municipio[0]->MUNI_telefono ?><br/>
                        <?php echo $municipio[0]->MUNI_mail ?><br/>
                        </div>
                	</td>
                	<td style="text-align:right;font-size: 9px;">
	                	
            		</td>
                </tr>
            </table>            
            
        </div>
        <div class="row" style="margin-top: 5px;padding: 2px 5px;font-size: 10px;margin-bottom:20px;">              
            <div class="col-new" style="width:712px" >
                <p style="margin:0 0 5px;line-height:normal;"><u>Titular</u>: <?php echo $registro[0]->resp ?></p>
                <p style="margin:0 0 5px;line-height:normal;"><u>Domicilio</u>: <?php echo $registro[0]->place ?></p>
                <p style="margin:0;line-height:normal;"><u>Identificación</u>: <?php echo $identificador ?> <?php echo $registro[0]->bien ?></p>
            </div>
        </div>
        <div class="row" style="margin-top:0;padding: 0px 5px;font-size: 12px;">              
            <div class="col-new" style="width:100%;height:200px;overflow-y: hidden">
                <table class="table table-mini-2 text-center" style="">
                    <thead>
                        <tr >
                            <th style="width:100px;text-align:center;">Cuota</th>
                            <th style="width:100px;text-align:center;">Vto.</th>
                            <th style="width:100px;text-align:center;">Cedulón</th>
                            <th style="width:100px;text-align:center;">Monto</th>
                            <th style="width:100px;text-align:center;">Plan Vinc.</th>
                            <th style="width:100px;text-align:center;">Recargo</th>
                            <th style="width:100px;text-align:center;">Honorarios</th>
                            <th style="width:100px;text-align:right;" class="text-right">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php echo $this->page_model->get_table_generator('',$limit,$start,$fecha,$condicion); ?>
                    </tbody>
                </table>

            </div>
        </div>
        <pagefooter name="MyFooter2" content-left="{DATE j-m-Y}"
        content-center="{PAGENO}/{nbpg}" footer-style="font-size: 8pt;">
        <htmlpagefooter name="MyFooter1">
        <div style="margin-top: 5px;font-size: 10px;padding-left: 1px;">
            <div class="row" style="margin: 0" >
                <div class="col-new" style="font-weight: bold;width: 712px;padding: 0;">
                    Procurador: <?php  echo $this->page_model->proName(); ?>
                </div>
            </div>
            <div class="row" style="" >
                <div class="col-new" style="width: 100%;">
                    <table class="table table-mini" style="border: 2px solid #000;">
                        <tbody>
                            <tr class="text-center">
                                <td class="text-left" style="width:200px"><div style="margin-left: 8px;margin-top: 0px;">COMPROBANTE N°</div></td>
                                <td style="font-size: 12px;font-weight: bold;width: 140px;border-right: 2px solid #000;border-left: 2px solid #000;">VENCIMIENTO</td>
                                <td style="font-weight: bolder;width: 110px;font-size: 12px;border-right: 2px solid #000;">
                                    <?php  echo $exp1 = $this->page_model->expDates('1', $fecha,$condicion) ?></td>
                                <td style="font-weight: bolder;width: 110px;font-size: 12px;border-right: 2px solid #000;">
                                    <?php  echo $exp2 = $this->page_model->expDates('2', $fecha,$condicion) ?></td>
                                <td style="font-weight: bolder;width: 110px;font-size: 12px;">
                                    <?php  echo $exp3 = $this->page_model->expDates('3', $fecha,$condicion) ?></td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-right" style="width:100px">
                                    <div style="font-weight: bolder;font-size: 20px;position: absolute;width: 200px;top: 10px;left:33px">
                                        <?php if($nextComprobante == 'si'): ?>
                                            <?php echo $this->page_model->getNextReceiptNum(TRUE); ?>
                                        <?php else: ?>
                                            <?php echo $this->page_model->getNextReceiptNum(TRUE); ?>
                                        <?php endif; ?>
                                    </div>
                                </td>
                                <td style="font-size: 12px;font-weight: bold;width: 140px;border-right: 2px solid #000;border-left: 2px solid #000;">MONTO</td>
                                <td style="font-weight: bolder;font-size: 12px;border-right: 2px solid #000;"><?php echo $dbt1 = $this->page_model->get_table_generator('total1', $limit,$start, $fecha,$condicion); ?></td>
                                <td style="font-weight: bolder;font-size: 12px;border-right: 2px solid #000;"><?php echo $dbt2 = $this->page_model->get_table_generator('total2', $limit,$start, $fecha,$condicion); ?></td>
                                <td style="font-weight: bolder;font-size: 12px;"><?php echo $dbt3 = $this->page_model->get_table_generator('total3', $limit,$start, $fecha,$condicion); ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table" style="max-width: 357px;">
                        <tbody>
                            <tr class="">
                                <td class="text-left" style="font-size: 10px;font-weight: bold;padding: 2px !important"><u>Lugares de Cobro</u>:</td>
                            </tr>
                            <tr class='text-center'>
                                <td style='font-size: 10px;padding: 2px !important'>
                                    <?php echo $this->page_model->comCode("html"); ?>
                                </td>
                            </tr>
                            <?php foreach($codigo as $cod): ?>
                                <?php if(!empty($cod->{'LUGA_identidad'})): ?>
                                    <tr style="text-align:center;">
                                        <td style='font-size: 10px;padding: 2px !important'>
                                            
                                            <?php echo $this->page_model->doBarcode($cod->{'LUGA_identidad'}, $cod->{'LUGA_nombre'}) ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <table class="table" style="max-width: 357px;margin-top: 15px">
                        <tbody>
                            <?php //echo comCode(); ?>                                      
                        </tbody>
                    </table>
                    <table class="table" style="width:100%;margin-top:20px;">
                        <tbody>
                            <tr>
                                <td colspan="2" class="text-left" style="padding-top: 2px !important;padding-bottom: 2px !important;width: 40%;text-align:left;font-size:12px;">
                                    El pago del presente no exime deudas anteriores
                                </td>
                                <td class="text-center" style="padding-top: 5px !important;padding-bottom: 5px !important;padding-right: 5px !important;padding-left: 5px !important;width: 30%;text-align:center;background:#000;">
                                    <div style="width: 100%;display:inline-block;height: 17px;margin: 10px 10px 10px 10px !important;background-color: #000;color: #fff;font-style: italic;">
                                        Sistema S.A.M. (C) TRILEXA
                                    </div>
                                </td>
                                <td class="text-right" style="font-style: italic;padding-top: 2px !important;padding-bottom: 2px !important;width: 30%;text-align:right;font-size:12px;">
                                    Talón para el contribuyente
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>                    
        </div>
        </htmlpagefooter>

        <sethtmlpagefooter name="MyFooter1" value="on" />
</div>
<?php $rNum = $this->page_model->getNextReceiptNum(FALSE); 
$this->page_model->receiptInsert($rNum, $exp1, $exp2, $exp3, $dbt1, $dbt2, $dbt3);
//die;

?>
<?php $municipio = $this->page_model->get_municipio(); ?>
<header style="padding-bottom:50px;">
<div class="row">
    <div class="col-3 mrightN10P hidden-sm hidden-xs">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">
                        <a href="/" title="Inicio">
                            <img class="img-fluid img-thumbnail " src="<?php echo base_url() ?>asset/img/logo_muni.png"/>
                            <!--<div class="muni_img"></div>-->
                        </a>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 hidden-md hidden-lg">
        <div class="m-subheader center-image">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3>
                        <a href="/" title="Inicio">
                            <img class="img-fluid img-thumbnail " src="<?php echo base_url() ?>asset/img/logo_muni.png"/>
                            <!--<div class="muni_img"></div>-->
                        </a>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3 col-md-6 mtop10 hidden-xs hidden-sm fixHeaderResponsive">
        <div id="rPNameDisplay">
            <h3 class="bold"><?php echo $municipio[0]->{'MUNI_nombre'} ?></h3>
            <h5>Dirección: <?php echo $municipio[0]->{'MUNI_calle'} ?></h5>
            <h5>C.P: <?php echo $municipio[0]->{'MUNI_codpos'} ?> - <?php echo $municipio[0]->{'LOCA_nombre'} ?> - <?php echo $municipio[0]->{'PCIA_nombre'} ?></h5>
            <h5>Email: <?php echo $municipio[0]->{'MUNI_mail'} ?></h5>
            <h5>Tel: <?php echo $municipio[0]->{'MUNI_telefono'} ?></h5>
        </div>
    </div>
    <div class="col-12 mtop10 hidden-md hidden-lg text-center">
        <div id="rPNameDisplay">
            <?php echo $municipio[0]->{'MUNI_calle'} ?>
            <h3 class="bold"><?php echo $municipio[0]->{'MUNI_nombre'} ?></h3>
            <h5></h5>
        </div>
    </div>
</div>
</header>
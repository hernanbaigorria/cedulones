
<footer class="m-grid__item m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-footer__wrapper">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--center m-stack__item--last">
                    <img alt="" src="<?php echo base_url() ?>asset/img/logo.png" class="mar-50 w190" />
                </div>
            </div>
        </div>
    </div>
</footer>
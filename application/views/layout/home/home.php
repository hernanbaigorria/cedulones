<div class="row">

<div class="col-xl-12">
    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption w100perc">
                <div class="m-portlet__head-title w100perc">
                    <h3 class="m-portlet__head-text w100perc dis-block text-center fz23rem">
                        Gobierno electrónico
                    </h3>
                    <h6> 
                    El gobierno electrónico es la aplicación de las tecnologías de la información y la comunicación al funcionamiento del sector público, con el objetivo de brindar mejores servicios al ciudadano e incrementar la eficiencia, la transparencia y la participación ciudadana.
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="col-xl-3">
    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
    <div class="m-portlet__body dis-block text-center style-icon-fas-col">
            <a href="<?php echo base_url() ?>deudas/">
                <i class="icon-fas fas fa-print"></i>
                <i class="icon-fas fas fa-dollar-sign"></i>
                <p class="icon-fas-text">Impresión y pagos de deudas</p>
            </a>
        </div>
    </div>
</div>
<div class="col-xl-3">
    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
        <div class="m-portlet__body dis-block text-center style-icon-fas-col">
            <a href="servicios/login_exp"> 
                <i class="icon-fas fas fa-archive"></i>
                <p class="icon-fas-text">Seguimiento de expedientes</p>
            </a>
        </div>
    </div>   
</div>
<div class="col-xl-3">
    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
        <div class="m-portlet__body dis-block text-center style-icon-fas-col">
            <a href="servicios/login_jur"> 
                <i class="icon-fas fas fa-file-alt"></i>
                <i class="icon-fas-sec fas fa-lock"></i>
                <p class="icon-fas-text">Presentación de declaración jurada</p>
            </a>
        </div>
    </div>
</div>
<div class="col-xl-3">
    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height m-portlet--rounded hover-bg-or">
        <div class="m-portlet__body dis-block text-center style-icon-fas-col">
            <a href="servicios/login_con"> 
                <i class="icon-fas fas fa-users"></i>
                <i class="icon-fas-sec fas fa-lock"></i>
                <p class="icon-fas-text">Contribuyente digital</p>
            </a>
        </div>
    </div>   
</div>
</div>
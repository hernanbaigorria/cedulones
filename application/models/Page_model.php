<?php
    class page_model extends CI_Model
    {
    
    
            public function __construct()
            {
                parent::__construct();
                // Your own constructor code
                $this->load->library('m_pdf');
                $this->load->helper('cookie');
            }
            
            public $title;
            public $content;
            public $date;



            function get_tableheader()
            {

                /* Funcion para generar la tabla en view/layout/deudas/detalle.php */

                $id = $this->input->cookie('identi',false);
                $type = $this->input->cookie('type',false);

                $aRegistro = $this->page_model->get_registros($id,$type);


                // Explode en los resultados para filtrarlos
                $bienO = explode("-", $aRegistro[0]->bien);
                $bien = explode("Lote:", $bienO[0]);

                
                // Fin explode

                // PROPIEDAD

                if($type == '1'){

                $identi = $aRegistro[0]->identi;
                $estado = ucwords(trim(str_replace("Estado:", "", $bienO[3])));
                $manz = "Mz." . trim(str_replace("Manzana:", "", $bien[0]));
                $lote = "Lt." . trim($bien[1]);
                $resp = ucwords($aRegistro[0]->resp);
                $place = ucwords(htmlentities($aRegistro[0]->place));
                $surface = '';

                $fullFill = '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay1">';
                $fullFill .= '<thead>';
                $fullFill .= '<tr>';
                $fullFill .= '<th scope="col" colspan="2">Datos del Bien</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Nomenclatura catastral</td>';
                $fullFill .= '<td>' . $identi . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Designación Oficial</td>';
                $fullFill .= '<td>' . $manz . " " . $lote . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Estado</td>';
                $fullFill .= '<td>' . $estado . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Superficie</td>';
                $fullFill .= '<td>' . $surface . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';
                $fullFill .= '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay2">';
                $fullFill .= '<thead><tr><th scope="col" colspan="2">Datos del Responsable de Pago</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Apellido y Nombres</td>';
                $fullFill .= '<td class="minw141">' . $resp . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr class="">';
                $fullFill .= '<td class="valign-middle maxw245">Dirección</td>';
                $fullFill .= '<td class="valign-middle">' . $place . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';

                }


                // AUTOMOTOR

                if($type == '2'){

                $identi = $aRegistro[0]->identi;
                $resp = ucwords($aRegistro[0]->resp);
                $place = ucwords(htmlentities($aRegistro[0]->place));
                $surface = '';

                $fullFill = '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay1">';
                $fullFill .= '<thead>';
                $fullFill .= '<tr>';
                $fullFill .= '<th scope="col" colspan="2">Datos del Bien</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Dominio</td>';
                $fullFill .= '<td>' . $identi . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Descripción</td>';
                $fullFill .= '<td>'.$aRegistro[0]->bien.'</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '</tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';
                $fullFill .= '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay2">';
                $fullFill .= '<thead><tr><th scope="col" colspan="2">Datos del Responsable de Pago</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Apellido y Nombres</td>';
                $fullFill .= '<td class="minw141">' . $resp . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr class="">';
                $fullFill .= '<td class="valign-middle maxw245">Dirección</td>';
                $fullFill .= '<td class="valign-middle">' . $place . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';

                }

                // COMERCIO

                if($type == '3'){

                $identi = $aRegistro[0]->identi;
                $resp = ucwords($aRegistro[0]->resp);
                $place = ucwords(htmlentities($aRegistro[0]->place));
                $surface = '';

                $fullFill = '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay1">';
                $fullFill .= '<thead>';
                $fullFill .= '<tr>';
                $fullFill .= '<th scope="col" colspan="2">Datos del Bien</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Comercio</td>';
                $fullFill .= '<td>' . $identi . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Nombre de fantasía</td>';
                $fullFill .= '<td>' . $aRegistro[0]->bien . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';
                $fullFill .= '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay2">';
                $fullFill .= '<thead><tr><th scope="col" colspan="2">Datos del Responsable de Pago</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Apellido y Nombres</td>';
                $fullFill .= '<td class="minw141">' . $resp . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr class="">';
                $fullFill .= '<td class="valign-middle maxw245">Dirección</td>';
                $fullFill .= '<td class="valign-middle">' . $place . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';

                }

                // CEMENTERIO

                if($type == '4'){

                $identi = $aRegistro[0]->identi;
                $estado = ucwords(trim(str_replace("Estado:", "", $bienO[3])));
                $manz = "Mz." . trim(str_replace("Manzana:", "", $bien[0]));
                $lote = "Lt." . trim($bien[1]);
                $resp = ucwords($aRegistro[0]->resp);
                $place = ucwords(htmlentities($aRegistro[0]->place));
                $surface = '';

                $fullFill = '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay1">';
                $fullFill .= '<thead>';
                $fullFill .= '<tr>';
                $fullFill .= '<th scope="col" colspan="2">Datos del Bien</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Cementerio</td>';
                $fullFill .= '<td>' . $identi . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Tipo</td>';
                $fullFill .= '<td>' . $aRegistro[0]->bien .'</td>';
                $fullFill .= '</tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';
                $fullFill .= '<div class="col-lg-5 col-sm-12 col-xs-12 center-block">';
                $fullFill .= '<table class="table table-bordered table-orange table_header_pay" id="tabpay2">';
                $fullFill .= '<thead><tr><th scope="col" colspan="2">Datos del Responsable de Pago</th>';
                $fullFill .= '</tr>';
                $fullFill .= '</thead>';
                $fullFill .= '<tbody>';
                $fullFill .= '<tr>';
                $fullFill .= '<td>Apellido y Nombres</td>';
                $fullFill .= '<td class="minw141">' . $resp . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '<tr class="">';
                $fullFill .= '<td class="valign-middle maxw245">Dirección</td>';
                $fullFill .= '<td class="valign-middle">' . $place . '</td>';
                $fullFill .= '</tr>';
                $fullFill .= '</tbody>';
                $fullFill .= '</table>';
                $fullFill .= '</div>';

                }

                return $fullFill;
            }
            
            // Funcion que construye el html. Recibe los datos desde la funcion rDataFill
            function rInmHtmlConstruc($tName, $aNumber, $expireComplete, $debtAmount,$recargos,$honorarios, $tDebtRateDebtPlus, $stateName, $cNumber, $tCuotas='', $pCuotas='', $i) {
                //--------------------------------------------------------------------------------------------------------------------
                //Construye tabla para ambas vista y mediante estilos muestra u oculta según el tamaño de la pantalla que se visualice
                //--------------------------------------------------------------------------------------------------------------------
                // Construye la tabla para una vista LG MD
                
                $return = "<tr class='hidden-sm hidden-xs' cuote='$tName $aNumber' debteach='$tDebtRateDebtPlus' id='cuote$i'>";
                if(!empty($tCuotas)):
                $return .= "<td class=''>PLAN $tName $tCuotas/$pCuotas</td>";
                else:
                $return .= "<td class=''>$tName $aNumber</td>";
                endif;
                $return .= "<td class=''>$expireComplete</td>";
                $return .= "<td class='' id=''>$ $debtAmount</td>";
                $return .= "<td class='' id=''>$ $recargos</td>";
                $return .= "<td class='' id=''>$ $honorarios</td>";
                $return .= "<td class='' id='debtCalc$i'>$ $tDebtRateDebtPlus</td>";
                $return .= "<td class=''>$stateName</td>";
                $return .= "<td class='text-center'>";
                if(!empty($tCuotas)):
                    $return .= "<input name='checkbox3[]' val-table='$aNumber' reg-number='$i' value='$tDebtRateDebtPlus' class='styled checkboxCalc' type='checkbox'>";
                    $return .= "</td>";
                    $return .= "<td class='hidden' id='aNumber$i'>";
                    $return .= $aNumber;
                    $return .= "</td>";
                else:
                    $return .= "<input name='checkbox3[]' val-table='$cNumber' reg-number='$i' value='$tDebtRateDebtPlus' class='styled checkboxCalc' type='checkbox'>";
                    $return .= "</td>";
                    $return .= "<td class='hidden' id='cNumber$i'>";
                    $return .= $cNumber;
                    $return .= "</td>";
                endif;
                $return .= "</tr>";

                // Con este pequeño hack, aumento a 1000 la variable $i porque debajo se construye una tabla nueva
                // De ésta manera puedo diferenciar bien los datos logrando que no se mezclen
                $i = (1000 + $i);
                // Construye la tabla para una vista SM XS
                $return .= "<tr class='hidden-lg hidden-md' cuote='$tName $aNumber' debteach='$tDebtRateDebtPlus' id='cuote$i'>";
                $return .= "<td class='text-left-new'>Cuota</td>";
                $return .= "<td class='padR10p'>$tName $aNumber</td>";
                $return .= "<td rowspan='4' class='table-checkbox'>";
                $return .= "<input name='checkbox4[]' reg-number='$i' value='$tDebtRateDebtPlus' class='styled checkboxCalc' type='checkbox'>";
                $return .= "</td>";
                $return .= "</tr>";
                $return .= "<tr class='hidden-lg hidden-md'>";
                $return .= "<td class='text-left-new'>Vencimiento</td>";
                $return .= "<td class='padR10p'>$expireComplete</td>";
                $return .= "</tr>";
                $return .= "<tr class='hidden-lg hidden-md'>";
                $return .= "<td class='text-left-new'>Monto</td>";
                $return .= "<td class='padR10p' id='debtCalc$i'>$ $tDebtRateDebtPlus</td>";
                $return .= "</tr>";
                $return .= "<tr class='hidden-lg hidden-md'>";
                $return .= "<td class='text-left-new'>Situación</td>";
                $return .= "<td class='padR10p'>$stateName</td>";
                $return .= "</tr>";
                $return .= "<tr class='hidden-lg hidden-md'>";
                $return .= "<td colspan='3' class='td-separator'></td>";
                $return .= "</tr>";
                $return .= "<tr class='hidden-lg hidden-md'>";
                $return .= "<td class='hidden' id='cNumber$i'>";
                $return .= $cNumber;
                $return .= "</td>";
                $return .= "</tr>";


                return $return;
            }

            public function get_bien_numero($id, $type) {

                $query = $this->db->query("SELECT Identificacion FROM zvistabienes WHERE Identificacion = '$id' AND BIEN_numero = '$type'");

                return $query->num_rows();

            }


            public function get_deduda_existente($id, $type) {

                $query2 =  $this->db->query("SELECT BIEN_bien FROM zvistadeudas WHERE BIEN_bien = '$id' AND BIEN_numero = '$type'");

                return $query2->num_rows();

            }
            

            public function daysSelect($num) {
                // $num cantidad de días máximos a mostrar.
                for ($i = 1; $i <= $num; $i++) {
                    ($_COOKIE["daysPlus"] == $i) ? $sel = "selected" : $sel = "";
                    $return .= "<option $sel value='$i'>$i día/s</option>";
                }
                echo $return;
            }

            public function expDates($date, $fecha= '', $condicion = 'no') {

                if (!isset($_COOKIE["daysPlus"]) || empty($_COOKIE["daysPlus"])) {
                    $_COOKIE["daysPlus"] = 0;
                }

                $daysPlus = $_COOKIE["daysPlus"];
                // Calculo las 3 fechas de vencimiento
                if($condicion == 'no'){
                    $fechaRecibo = date('Ymd');
                } else {
                    $fechaRecibo = $fecha;
                }
                $dateExp1 = date('Ymd', strtotime($fechaRecibo . " +$daysPlus day"));
                $dateExp2 = date('Ymd', strtotime($dateExp1 . " +10 day"));
                $dateExp3 = date('Ymd', strtotime($dateExp2 . " +10 day"));

                $dateExp1 = date("d/m/Y", strtotime($dateExp1));
                $dateExp2 = date("d/m/Y", strtotime($dateExp2));
                $dateExp3 = date("d/m/Y", strtotime($dateExp3));
                switch ($date):
                    case "1": return $dateExp1;
                        break;
                    case "2": return $dateExp2;
                        break;
                    case "3": return $dateExp3;
                        break;
                endswitch;
            }
            
            public function exportarPDF($id, $selecciondos, $ndeuda)
            {
                date_default_timezone_set('America/Argentina/Buenos_Aires');

                //now pass the data//
                 
                 //now pass the data //
                
                //actually, you can pass mPDF parameter on this load() function
                $pdf = new Mpdf('c', 'A4');
                
                $pdf_style = '
                    <style media="print">
                    body, html {padding:12px;margin:0;}
                    .content-info {border: 1px solid #e6132a;border-radius: 30px;padding:8px 20px 0 0;font-family: Arial, sans-serif;margin-bottom: 5px;}
                    .content_grla {}
                </style>';
                
                $pdf->SetMargins(0,0,0);
                $pdf->SetDisplayMode('fullpage');

                /*$pdf->SetHTMLHeader('<div style="text-align:right">B I R A</div>');*/
                /*$pdf->setFooter(' | www.bira.com | {PAGENO}');*/
                
                
                $id= $this->input->cookie('identi',false);
                $ndeuda = json_decode($_COOKIE['numdeuda'], false);

                date_default_timezone_set('America/Argentina/Buenos_Aires');
                $this->db->select("ANPE_vencimiento");
                $this->db->where('BIEN_bien', $id);
                $this->db->group_start();
                foreach ($ndeuda as $key => $passCedu) {
                    $this->db->or_like("DEUD_numero", $passCedu);
                }
                $this->db->group_end();

                $queyVenco = $this->db->get("zvistadeudas");
                $numeroDeudaVencimiento = $queyVenco->result();
                foreach ($numeroDeudaVencimiento as $fecha) {
                    if($fecha->ANPE_vencimiento >= date("Y-m-d")){

                        $fechaAdelantada[] = $fecha->ANPE_vencimiento;

                        $dataLimit = array();
                        $data['title']="Cedululon";
                        $data['description']="";

                        $dataAdelantada['condicion'] = 'si';
                        $dataAdelantada['fecha']= $fecha->ANPE_vencimiento;

                        $dataAdelantada['municipio'] = $this->get_municipio();
                        $dataAdelantada['registro'] = $this->get_registro_id($id);
                        $dataAdelantada['identificador'] = $id;
                        $dataAdelantada['selecciondos'] = $selecciondos;
                        $dataAdelantada['ndeuda'] = $ndeuda;
                        $dataAdelantada['codigo'] = $this->codigos_de_barras();
                        
                        $dataAdelantada['limit'] = 12;

                        $dataAdelantada['start'] = 0;

                        $pdf_page_adelantada = $this->load->view('layout/pdf/pdf_v3',$dataAdelantada,true);
                        $pdf->AddPage();
                        $pdf->WriteHTML($pdf_style.$pdf_page_adelantada);
                    } 
                }

                $contarFechaAdelantada = count($fechaAdelantada);

                $limit= 30;
                $count = $_COOKIE['countSelected'];

                for ($i = 1; $i <= $count; $i++) {
                    // Si la cantidad de resultados es múltiplo de 12
                    

                    if ($i % $limit == 0) {


                        $isRest = FALSE;
                        // Calculo desde donde comenzar al seleccion en la DB
                        $start = $i - $limit;
                        // Contador: cuantas veces completé el limite
                        $c = $c + 1;
 
                        $file = $start;
                        $data = array();

                        $data['title']="Cedululon";
                        $data['description']="";
                        
                        $data['condicion'] = 'no';
                        $data['fecha']= $fechaAdelantada;

                        $next = FALSE;

                        $data['municipio'] = $this->get_municipio();
                        $data['registro'] = $this->get_registro_id($id);
                        $data['identificador'] = $id;
                        $data['selecciondos'] = $selecciondos;
                        $data['ndeuda'] = $ndeuda;
                        $data['codigo'] = $this->codigos_de_barras();

                        $data['limit'] = $limit;
                        $data['start'] = $file;
                        
                        $pdf_page = $this->load->view('layout/pdf/pdf_v3',$data,true);
                        $pdf->WriteHTML($pdf_style.$pdf_page);

                        $data['nombre'][$i] = 'nombre_'.$i;
                        
                    } else {
                        // Calculo el ultimo registros que ya fueron consultados para saber desde donde empezar
                        $isRest = TRUE;
                        $next = TRUE;
                        $start = ($limit * $c);
                        // Calculo cuantos registro restan calcular
                        $file_2 = $start;
                    }


                }

                if($isRest){

                    $dataLimit = array();

                    $data['title']="Cedululon";
                    $data['description']="";

                    $dataLimit['condicion'] = 'no';
                    $dataLimit['fecha']= $fechaAdelantada;

                    $dataLimit['municipio'] = $this->get_municipio();
                    $dataLimit['registro'] = $this->get_registro_id($id);
                    $dataLimit['identificador'] = $id;
                    $dataLimit['selecciondos'] = $selecciondos;
                    $dataLimit['ndeuda'] = $ndeuda;
                    $dataLimit['codigo'] = $this->codigos_de_barras();
                    
                    $dataLimit['limit'] = $limit;

                    $dataLimit['start'] = $file_2;

                    if($next){
                        $dataLimit['nextComprobante'] = 'si';
                    }

                    $pdf_page_limit = $this->load->view('layout/pdf/pdf_v3',$dataLimit,true);
                    $pdf->AddPage();
                    $pdf->WriteHTML($pdf_style.$pdf_page_limit);


                                        
                }

                
                $name = "cedulones_".time().".pdf";
                
                // save file
                $pdf->Output(dirname(__FILE__)."/../../uploads/".$name,'F');

                //return $pdf->Output(dirname(__FILE__)."/../../uploads/".$name,'F');
                $data['nombre'] = $name;
                $data['url'] = base_url()."uploads/".$name;
                echo json_encode($data);
                exit;
                //$dwn = $pdf->Output($name,"I");
                //echo $dwn;
                


            }



            function getMonthAmountDays($YearMonth = '', $option = 'returnDays') {
                if ($option == 'returnDays') {
                    // Separo el año y el mes
                    $year = substr($YearMonth, 0, 4);
                    $month = substr($YearMonth, 4, 6);

                    // Esta función me devuelve la cantidad de días que tiene X mes, el año que se indique
                    //print_r($month);
                    //exit;
                    $return = cal_days_in_month(CAL_GREGORIAN, $month, $year);

                } else if ($option == 'returnDaysPlus') {
                    // Manejo la cantidad de días con cookies
                    // Si no está definida, por predeterminado es 7 
                    if (!isset($_COOKIE["daysPlus"]) || empty($_COOKIE["daysPlus"])) {
                        $_COOKIE["daysPlus"] = 0;
                    }
                    $amountD = $_COOKIE["daysPlus"];
                    $return = $amountD;
                }
                return $return;
            }

            function calcFee($do, $feeRate, $debtAmount) {
                $return = "0";
                if ($do == '2') {
                    $return = ($debtAmount * $feeRate) / 100;
                }
                return $return;
            }

            // Funcion que dar formato de plata
            function moneyFormat($number, $format = "%.2n") {
                $regex = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?' .
                        '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
                if (setlocale(LC_MONETARY, 0) == 'C') {
                    setlocale(LC_MONETARY, '');
                }
                $locale = localeconv();
                preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
                foreach ($matches as $fmatch) {
                    $value = floatval($number);
                    $flags = array(
                        'fillchar' => preg_match('/\=(.)/', $fmatch[1], $match) ?
                        $match[1] : ' ',
                        'nogroup' => preg_match('/\^/', $fmatch[1]) > 0,
                        'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
                        $match[0] : '+',
                        'nosimbol' => preg_match('/\!/', $fmatch[1]) > 0,
                        'isleft' => preg_match('/\-/', $fmatch[1]) > 0
                    );
                    $width = trim($fmatch[2]) ? (int) $fmatch[2] : 0;
                    $left = trim($fmatch[3]) ? (int) $fmatch[3] : 0;
                    $right = trim($fmatch[4]) ? (int) $fmatch[4] : $locale['int_frac_digits'];
                    $conversion = $fmatch[5];

                    $positive = true;
                    if ($value < 0) {
                        $positive = false;
                        $value *= -1;
                    }
                    $letter = $positive ? 'p' : 'n';

                    $prefix = $suffix = $cprefix = $csuffix = $signal = '';

                    $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
                    switch (true) {
                        case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
                            $prefix = $signal;
                            break;
                        case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
                            $suffix = $signal;
                            break;
                        case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
                            $cprefix = $signal;
                            break;
                        case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
                            $csuffix = $signal;
                            break;
                        case $flags['usesignal'] == '(':
                        case $locale["{$letter}_sign_posn"] == 0:
                            $prefix = '(';
                            $suffix = ')';
                            break;
                    }
                    $space = $locale["{$letter}_sep_by_space"] ? ' ' : '';

                    $value = number_format($value, $right, $locale['mon_decimal_point'], $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
                    $value = @explode($locale['mon_decimal_point'], $value);

                    $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
                    if ($left > 0 && $left > $n) {
                        $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
                    }
                    $value = implode($locale['mon_decimal_point'], $value);
                    if ($locale["{$letter}_cs_precedes"]) {
                        $value = $prefix . $currency . $space . $value . $suffix;
                    } else {
                        $value = $prefix . $value . $space . $currency . $suffix;
                    }
                    if ($width > 0) {
                        $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
                                STR_PAD_RIGHT : STR_PAD_LEFT);
                    }

                    $format = str_replace($fmatch[0], $value, $format);
                }
                return $format;
            }

            // Funcion que puede devolver los interes o montos de los 3 vtos
            function rCalcRateDebtsForExpires($rateOrDebts = "rate", $debtsAmount = '0', $rRates = '0', $fechaVencimiento = false) {

                

                // $paramType: Tipo de bien
                // $rateOrDebts: Opciones['rate', 'debts']. Que devuelvo? Intereses o Monto ya calculados.
                // $debtsAmount: Para el caso que $rateOrDebts = 'debts', le paso el monto sobre el cual calcular los intereses
                // $rRates: Cuales rates quiero devolver. Por defecto devuelve el 2do y 3ero
                // ------------------------------------------------------------------------------------------------------------
                $type = $this->input->cookie('type',false);
                if (!isset($_COOKIE["daysPlus"]) || empty($_COOKIE["daysPlus"])) {
                    $_COOKIE["daysPlus"] = 0;
                }

                $daysPlus = $_COOKIE["daysPlus"];


                // $aYMFE = Year Month First Expire
                // $aYMSE = Year Month Second Expire
                // $aYMTE = Year Month Third Expire
                // ----------------------------------
                // FE = First Expire
                // SE = Second Expire
                // TE = Third Expire
                if($fechaVencimiento){
                    $fechaVencimientoRecibo = $fechaVencimiento;
                } else {
                    $fechaVencimientoRecibo = date('Ymd');
                }
                $aYMFE = date('Ymd', strtotime($fechaVencimientoRecibo . " +$daysPlus day"));
                $aYMSE = date('Ymd', strtotime($aYMFE . " +10 day"));
                $aYMTE = date('Ymd', strtotime($aYMSE . " +10 day"));


                // Corto la cadena para solo devolver el año|mes
                $aYMFE = substr($aYMFE, 0, 6);
                $aYMSE = substr($aYMSE, 0, 6);
                $aYMTE = substr($aYMTE, 0, 6);


                // Armo la consulta para FE
                $this->db->select('FECH_interes as rate');
                $this->db->select('FECH_anomes as yearMonth');

                //$this->db->where('BIEN_numero', $type);
                $this->db->where('FECH_anomes', $aYMFE);
                $query1 = $this->db->get('fechasinteres');
                $aRegFE = $query1->result();
                

                $this->db->select('FECH_interes as rate');
                $this->db->select('FECH_anomes as yearMonth');

                //$this->db->where('BIEN_numero', $type);
                $this->db->where('FECH_anomes', $aYMSE);
                $query2 = $this->db->get('fechasinteres');
                $aRegSE = $query2->result();


                $this->db->select('FECH_interes as rate');
                $this->db->select('FECH_anomes as yearMonth');

                //$this->db->where('BIEN_numero', $type);
                $this->db->where('FECH_anomes', $aYMTE);
                $query3 = $this->db->get('fechasinteres');
                $aRegTE = $query3->result();        

                // Obtengo los datos de la consulta
                // Calculo los montos + 7 días de intereses

               // print_r($rateFE);
                $rateFE = $aRegFE[0]->rate;
                $yearMonthFE = $aRegFE[0]->yearMonth;
                $daysFE = $this->getMonthAmountDays($yearMonthFE);
                $rateCalcFE = (($daysPlus * $rateFE) / $daysFE);

                // Calculo los montos + 10 días de intereses
                $rateSE = $aRegSE[0]->rate;
                $yearMonthSE = $aRegSE[0]->yearMonth;
                $daysSE = $this->getMonthAmountDays($yearMonthSE);

                $rateCalcSE = ((10 * $rateSE) / $daysSE);

                // Calculo los montos + 10 días de SE y + 10 días de TE días de intereses (20 días en total)
                $rateTE = $aRegTE[0]->rate;
                $yearMonthTE = $aRegTE[0]->yearMonth;
                $daysTE = $this->getMonthAmountDays($yearMonthTE);
                $rateCalcTE = $rateCalcSE + ((10 * $rateTE) / $daysTE);

                // Retorno los resultados al AJAX
                // Devuelvo solos los intereses
                if ($rateOrDebts === "rate") {
                    // En caso que necesite obtener los 3 rates
                    if ($rRates === 3) {
                        return bcdiv($rateCalcFE, 1, 2) . '|' . bcdiv($rateCalcSE, 1, 2) . "|" . bcdiv($rateCalcTE, 1, 2);
                    } else {
                        return $rateCalcSE . "|" . $rateCalcTE;
                    }
                }
                // Devuelvo el monto con los intereses ya aplicados
                if ($rateOrDebts === "debts") {
                    $debtCalcSE = $debtsAmount + (($debtsAmount * $rateCalcSE) / 100);
                    $debtCalcTE = $debtsAmount + (($debtsAmount * $rateCalcTE) / 100);
                    return $debtCalcSE . "|" . $debtCalcTE;
                }
            }


            function receiptDetNum($rNum, $dNum, $aNum, $dExp, $dAmount, $dCharges, $dFee, $planNumero) {
                // Conexion a la DB
                // Vuelvo a dar formato a las fechas para que respeten los de la base
                $dExp = str_replace("/", "-", $dExp);

                // Reemplazo las comas por puntos
                $dCharges = str_replace(",", ".", $dCharges);
                $dFee = str_replace(",", ".", $dFee);

                // Creo el INSERT para recibosdetalle y lo ejecuto
                $data = array(
                    'RECI_numero' => $rNum,
                    'DEUD_numero' => $dNum,
                    'ANPE_numero' => $aNum,
                    'DEUD_vencimiento' => $dExp,
                    'DEUD_monto' => $dAmount,
                    'DEUD_recargo' => $dCharges,
                    'DEUD_honorarios' => $dFee,
                    'DEUD_vinculado' => 0,
                     'PLAN_numero' => $planNumero
                );

                $this->db->insert('recibosdetalle', $data);
            }

            public function get_table_generator($total,$limit=null,$start=null,$fecha = array(),$whereFecha = 'no',$rdetails = true)
            {       
                $id= $this->input->cookie('identi',false);
                $type = $this->input->cookie('type',false);
                $selecciondos = json_decode($_COOKIE['selecciondos'], false);
                $ndeuda = json_decode($_COOKIE['numdeuda'], false);
            
                date_default_timezone_set('America/Argentina/Buenos_Aires');
                $this->db->select("CONCAT(TITA_abrev, ' / ', ANPE_numero) as quote");
                $this->db->select("ANPE_numero as a_num");
                $this->db->select("ANPE_vencimiento as expire_complete");
                $this->db->select("ANPE_vencimiento as expire_complete_traditional");
                $this->db->select("DEUD_monto as debt_amount");
                $this->db->select("DEUD_numero as c_number");
                $this->db->select("SITU_numero as situation");
                $this->db->select("PROC_nombre as proc_name");
                $this->db->select("PROC_extrajud as situation_rate");
                $this->db->select("YEAR(ANPE_vencimiento) as expire_year");
                $this->db->select("MONTH(ANPE_vencimiento) as expire_month");
                $this->db->select("DAY(ANPE_vencimiento) as expire_day");
                $this->db->where('BIEN_bien', $id);
                $this->db->group_start();
                foreach ($ndeuda as $key => $passCedu) {
                    $this->db->or_like("DEUD_numero", $passCedu);

                }
                
                $this->db->group_end();

                /* Si la condicion whereFecha es igual a "si" es porque hay que inculuir en la
                generacion del pdf la fecha que supera la actual */

                if($whereFecha == 'si'){
                    if(!empty($fecha)){
                        $this->db->where("ANPE_vencimiento", $fecha);
                    }
                }
                if($whereFecha == 'no'){
                    if(!empty($fecha)){
                        foreach ($fecha as $fechaWhere) {
                            $this->db->where_not_in("ANPE_vencimiento", $fechaWhere);
                        }
                    }
                }

                // Con este AND excluyo traer los registros que estan en la base con el monto total del 2019
                $this->db->not_like('ANPE_nombre',"TOTAL AÑO");
                //------------------------------------------------------------------------------------------
                $this->db->order_by("ANPE_vencimiento", "desc");

                $this->db->limit($limit,$start);

                $query = $this->db->get("zvistadeudas");
                if ($query->num_rows() > 0)
                {
                    $registro = $query->result_array();
                                       
                } else {
                    $registro =0;
                }

                // $z = Variable utilzada solo para cuando $total = partial. Ver función más abajo
                $z = 0;
                if($registro != 0){
                    foreach($registro as $key => $value){
                        $expireYearMonth = $registro[$key]['expire_year'].$registro[$key]['expire_month'];
                        $expireDay = $registro[$key]['expire_day'];
                        $expireComplete = date('d/m/Y',strtotime($registro[$key]['expire_complete']));
                        $expireCompleteTraditional = date('Y/m/d',strtotime($registro[$key]['expire_complete_traditional']));
                        $quote = $registro[$key]['quote'];
                        $aNum = $registro[$key]['a_num'];
                        $ceduNum = $registro[$key]['c_number'];
                        $situation = $registro[$key]['situation'];
                        $situationRate = $registro[$key]['situation_rate'];
                        $debtAmount = $registro[$key]['debt_amount'];

                    
                        // Busco los resultados cuyo año|mes sea mayor o igual al año|mes de vencimiento.
                        date_default_timezone_set('America/Argentina/Buenos_Aires');
                        $this->db->select('FECH_interes as rate');
                        $this->db->select('FECH_anomes as iexpire_year_month');
                        $this->db->where("BIEN_numero", $type);
                        $this->db->where('FECH_anomes >='.$expireYearMonth);
                        $fechaActual = date('Ym');
                        $this->db->where('FECH_anomes <='.$fechaActual);
                        $this->db->order_by('FECH_anomes', 'desc');
                        $query2 = $this->db->get('fechasinteres');

                        if ($query2->num_rows() > 0)
                        {
                            $fechasinteres = $query2->result_array();
                                               
                        }

                        $rateTotalT = 0;
                        $z++;

                        if($fechasinteres != NULL):
                        foreach ($fechasinteres as $key => $value) {
                        // $rate = Interes
                        // $iexpireYearMonth = Año|Mes fechasinteres
                        $rate = $fechasinteres[$key]['rate'];
                        $iexpireYearMonth = $fechasinteres[$key]['iexpire_year_month'];
                        //var_dump('fecha: '.$int->iexpire_year_month);
                        // Mediante la funcion getMonthAmountDays y el parametro 'returnDaysPlus' devuelvo la cantidad de días que deseo adiccionar como intereses diarios
                        // Al valor final total
                        $daysPlus = $this->getMonthAmountDays($iexpireYearMonth, 'returnDaysPlus');


                        $expireCompare = strtotime($expireCompleteTraditional);
                        $todayPlus7 = strtotime(date("Y/m/d") . "+ $daysPlus days");

                        if ($expireCompare <= $todayPlus7) {
                            // Por defecto $rateCalc tiene el valor devuelto de la consulta
                            $rateCalc = $rate;
                            // Mediante la funcion getMonthAmountDays devuelvo la cantidad de días que tiene un X mes, un X año
                            $days = $this->getMonthAmountDays($iexpireYearMonth);

                            // Si el año|mes de fechasinteres coincide con el año|mes de vencimiento
                            // O si el año|mes de fechasinteres coincide con el año|mes corriente
                            // Calculo la cantidad de días restantes del mes para así poder determinar el interes diario
                            $iexpireYearMonthF = strtotime(date("Ymd", strtotime($iexpireYearMonth . "01")));
                            $expireYearMonthF = substr(str_replace("/", "", $expireCompleteTraditional), 0, 6) . "01";
                            $expireYearMonthF = strtotime(date("Ymd", strtotime($expireYearMonthF)));

                            if ($iexpireYearMonthF == $expireYearMonthF || $iexpireYearMonth == DATE('Ym')) {
                                // Si el año|mes de fechainteres es igual al año|mes actual
                                if ($iexpireYearMonth == DATE('Ym')) {
                                    // Calculo el interes diario del prinicipio del mes actual al día de la fecha
                                    $rateCalc = ((DATE('d') * $rate) / $days);
                                } else {
                                    // En caso que el año|mes de fechasinteres coincide con el año|mes de vencimiento
                                    // Le resto al dia de vencimiento el total de días del mes de vencimiento para así calcular el interes diario
                                    $rateCalc = ((($days - $expireDay) * $rate) / $days);
                                }
                            }
                            // Hago una segunda validación y solo en el caso que sea el último mes, por ende el mes actual, 
                            // Le sumo X días de interéses determinados en la funcion getMonthAmountDays con el parámetro 'returnDaysPlus'
                            $ratePlus = ($daysPlus * $rate) / $days;
                            $rateCalc = $rateCalc + $ratePlus;
                        } else {
                            $rateCalc = '0';
                        }

                        //var_dump($rateTotalT);

                        // Cálculo del valor de intereses por cada deuda
                        $rateTotal = (($debtAmount * $rateCalc) / 100);

                        // Suma total de los valores de intereses
                        $rateTotalT = $rateTotalT + $rateTotal;

                        //var_dump($iexpireYearMonth);
                        //break;

                        }

                        else:
                            $rateTotalT = 0;
                        endif;


                        // Calculo de honorarios
                        $fee = $this->moneyFormat(bcdiv($this->calcFee($situation, $situationRate, $rateTotalT + $debtAmount), 1, 2));

                        // Suma total de los valores de honorarios
                        $feeTotal = bcdiv($feeTotal + $this->calcFee($situation, $situationRate, $rateTotalT + $debtAmount), 1, 2);

                        // Suma total de los montos de recargo. Doy formato de moneda
                        $charges = $this->moneyFormat(bcdiv($rateTotalT, 1, 2));

                        
                        // Inserto los datos en recibdosdetalle

                        

                        // Suma del valor total de intereses más la deuda, más los X días que se agregan, más los honorarios. Corto en 2 decimales. Doy formato de moneda
                        $tDebtRateDebtPlus = bcdiv($rateTotalT + $debtAmount + $this->calcFee($situation, $situationRate, $rateTotalT + $debtAmount), 1, 2);
                        // En caso que $total este vacio, solo construyo la tabla con todos los resultados
                        if ($total == "") {
                            $rNum = $this->page_model->getNextReceiptNum(FALSE) + 1;
                            $this->receiptDetNum($rNum, $ceduNum, $aNum, $expireCompleteTraditional, $debtAmount, $charges, $fee, 0);
                            // Doy formato de moneda
                            $debtAmountN = $this->moneyFormat($debtAmount);
                            $tDebtRateDebtPlusN = $this->moneyFormat($tDebtRateDebtPlus);
                            $return = "<tr>
                                                    <td style='text-align:center;font-size:13px;'>$quote</td>
                                                    <td style='text-align:center;font-size:13px;'>$expireComplete</td>
                                                    <td style='text-align:center;font-size:13px;'>$ceduNum</td>
                                                    <td style='text-align:center;font-size:13px;'>$debtAmountN</td>
                                                    <td style='text-align:center;font-size:13px;'>0,00</td>
                                                    <td style='text-align:center;font-size:13px;'>$charges</td>
                                                    <td style='text-align:center;font-size:13px;'>$fee</td>
                                                    <td style='text-align: right;font-size:13px;'>$tDebtRateDebtPlusN</td>
                                                </tr>";
                            echo $return;
                        }
                        // Si $total trae como valor "partial". Devuelvo datos parciales.
                        if ($total == "partial") {
                            // Realizo un pequeño hack para saber si $z es par o impar
                            // y así poder armar la tabla correcta y no mostrar resultados duplicados
                            ($z % 2 == 0) ? $return = "<tr><td style='text-align:center;font-size:9px;'>$quote</td><td style='text-align:center;font-size:9px;'>$ceduNum</td>" : $return = "<td style='text-align:center;font-size:9px;'>$quote</td><td style='text-align:center;font-size:9px;'>$ceduNum</td></tr>";
                            $z++;
                            echo $return;
                        }

                        // Sumo todos los totales de cada cuota
                        $debtTotal = $debtTotal + $tDebtRateDebtPlus;

                    }
                }


                $this->db->select("CONCAT('PLAN',' ', PLAN_numero, PLAN_totalcuotas, '/', PLAN_cuota) as quote");
                $this->db->select("PLAN_numero as t_name");
                $this->db->select("PLAN_vencimiento as expire_complete");
                $this->db->select("PLAN_vencimiento as expire_complete_traditional");
                $this->db->select("PLAN_numero as c_number");
                $this->db->select("PLAN_cedulon as a_num");
                $this->db->select("PLAN_totalcuotas as t_cuotas");
                $this->db->select("PLAN_cuota as p_cuotas");
                $this->db->select("SITU_nombre as state_name");
                $this->db->select("SITU_numero as situation");
                $this->db->select("PROC_extrajud as situation_rate");
                $this->db->select("YEAR(PLAN_vencimiento) as expire_year");
                $this->db->select("MONTH(PLAN_vencimiento) as expire_month");
                $this->db->select("DAY(PLAN_vencimiento) as expire_day");
                $this->db->select("PLAN_monto as debt_amount");

                $this->db->where("BIEN_bien", $id);

                $this->db->group_start();
                foreach ($ndeuda as $key => $passCedu) {
                    $this->db->or_like("PLAN_cedulon", $passCedu);
                }
                $this->db->group_end();

                if($whereFecha == 'si'){
                    if(!empty($fecha)){
                        //foreach ($fecha as $fechaWhere) {
                        $this->db->where("PLAN_vencimiento", $fecha);
                        //}
                    }
                }
                if($whereFecha == 'no'){
                    if(!empty($fecha)){
                        foreach ($fecha as $fechaWhere) {
                            //print_r($fechaWhere);
                            $this->db->where_not_in("PLAN_vencimiento", $fechaWhere);
                        }
                    }
                }

                $this->db->order_by('PLAN_vencimiento', 'desc');

                $this->db->limit($limit,$start);

                $queryPlanes = $this->db->get('zvistaplanes');

                if ($queryPlanes->num_rows() > 0)
                {
                    $registroPlanes = $queryPlanes->result_array();
                                       
                } else {
                    $registroPlanes = 0;
                }

                // $z = Variable utilzada solo para cuando $total = partial. Ver función más abajo
                $z = 0;

                if($registroPlanes != 0){

                    foreach($registroPlanes as $key => $value){
                        $expireYearMonth = $registroPlanes[$key]['expire_year'].$registroPlanes[$key]['expire_month'];
                        $expireDay = $registroPlanes[$key]['expire_day'];
                        $expireComplete = date('d/m/Y',strtotime($registroPlanes[$key]['expire_complete']));
                        $expireCompleteTraditional = date('Y/m/d',strtotime($registroPlanes[$key]['expire_complete_traditional']));
                        $quote = $registroPlanes[$key]['quote'];
                        $aNum = $registroPlanes[$key]['a_num'];
                        $ceduNum = $registroPlanes[$key]['c_number'];
                        $situation = $registroPlanes[$key]['situation'];
                        $situationRate = $registroPlanes[$key]['situation_rate'];
                        $debtAmount = $registroPlanes[$key]['debt_amount'];

                        // iexpire_year_month = Año y mes de fechasinteres
                        // Busco los resultados cuyo año|mes sea mayor o igual al año|mes de vencimiento.
                        date_default_timezone_set('America/Argentina/Buenos_Aires');
                        $this->db->select('FECH_interes as rate');
                        $this->db->select('FECH_anomes as iexpire_year_month');
                        $this->db->where("BIEN_numero", $type);
                        $this->db->where('FECH_anomes >='.$expireYearMonth);
                        $fechaActual = date('Ym');
                        $this->db->where('FECH_anomes <='.$fechaActual);
                        $this->db->order_by('FECH_anomes', 'desc');
                        $query2 = $this->db->get('fechasinteres');

                        if ($query2->num_rows() > 0)
                        {
                            $fechasinteres = $query2->result_array();
                                               
                        }

                        $rateTotalT = 0;

                        //$z++;
                        if($fechasinteres != NULL):
                        foreach ($fechasinteres as $key => $value) {
                        // $rate = Interes
                        // $iexpireYearMonth = Año|Mes fechasinteres
                        $rate = $fechasinteres[$key]['rate'];
                        $iexpireYearMonth = $fechasinteres[$key]['iexpire_year_month'];
                        //var_dump('fecha: '.$int->iexpire_year_month);
                        // Mediante la funcion getMonthAmountDays y el parametro 'returnDaysPlus' devuelvo la cantidad de días que deseo adiccionar como intereses diarios
                        // Al valor final total
                        $daysPlus = $this->getMonthAmountDays($iexpireYearMonth, 'returnDaysPlus');


                        $expireCompare = strtotime($expireCompleteTraditional);
                        $todayPlus7 = strtotime(date("Y/m/d") . "+ $daysPlus days");

                        if ($expireCompare <= $todayPlus7) {
                            // Por defecto $rateCalc tiene el valor devuelto de la consulta
                            $rateCalc = $rate;
                            // Mediante la funcion getMonthAmountDays devuelvo la cantidad de días que tiene un X mes, un X año
                            $days = $this->getMonthAmountDays($iexpireYearMonth);

                            // Si el año|mes de fechasinteres coincide con el año|mes de vencimiento
                            // O si el año|mes de fechasinteres coincide con el año|mes corriente
                            // Calculo la cantidad de días restantes del mes para así poder determinar el interes diario
                            $iexpireYearMonthF = strtotime(date("Ymd", strtotime($iexpireYearMonth . "01")));
                            $expireYearMonthF = substr(str_replace("/", "", $expireCompleteTraditional), 0, 6) . "01";
                            $expireYearMonthF = strtotime(date("Ymd", strtotime($expireYearMonthF)));

                            if ($iexpireYearMonthF == $expireYearMonthF || $iexpireYearMonth == DATE('Ym')) {
                                // Si el año|mes de fechainteres es igual al año|mes actual
                                if ($iexpireYearMonth == DATE('Ym')) {
                                    // Calculo el interes diario del prinicipio del mes actual al día de la fecha
                                    $rateCalc = ((DATE('d') * $rate) / $days);
                                } else {
                                    // En caso que el año|mes de fechasinteres coincide con el año|mes de vencimiento
                                    // Le resto al dia de vencimiento el total de días del mes de vencimiento para así calcular el interes diario
                                    $rateCalc = ((($days - $expireDay) * $rate) / $days);
                                }
                            }
                            // Hago una segunda validación y solo en el caso que sea el último mes, por ende el mes actual, 
                            // Le sumo X días de interéses determinados en la funcion getMonthAmountDays con el parámetro 'returnDaysPlus'
                            $ratePlus = ($daysPlus * $rate) / $days;
                            $rateCalc = $rateCalc + $ratePlus;
                        } else {
                            $rateCalc = '0';
                        }

                        //var_dump($rateTotalT);

                        // Cálculo del valor de intereses por cada deuda
                        $rateTotal = (($debtAmount * $rateCalc) / 100);

                        // Suma total de los valores de intereses
                        $rateTotalT = $rateTotalT + $rateTotal;

                        //var_dump($iexpireYearMonth);
                        //break;

                        }
                        else:
                            $rateTotalT = 0;
                        endif;


                        // Calculo de honorarios
                        $fee = $this->moneyFormat(bcdiv($this->calcFee($situation, $situationRate, $rateTotalT + $debtAmount), 1, 2));

                        // Suma total de los valores de honorarios
                        $feeTotal = bcdiv($feeTotal + $this->calcFee($situation, $situationRate, $rateTotalT + $debtAmount), 1, 2);

                        // Suma total de los montos de recargo. Doy formato de moneda
                        $charges = $this->moneyFormat(bcdiv($rateTotalT, 1, 2));

                        
                        // Inserto los datos en recibdosdetalle

                        

                        // Suma del valor total de intereses más la deuda, más los X días que se agregan, más los honorarios. Corto en 2 decimales. Doy formato de moneda
                        $tDebtRateDebtPlus = bcdiv($rateTotalT + $debtAmount + $this->calcFee($situation, $situationRate, $rateTotalT + $debtAmount), 1, 2);
                        // En caso que $total este vacio, solo construyo la tabla con todos los resultados
                        if ($total == "") {
                            $rNum = $this->page_model->getNextReceiptNum(FALSE) + 1;
                            $this->receiptDetNum($rNum, $ceduNum, $aNum, $expireCompleteTraditional, $debtAmount, $charges, $fee, $ceduNum);
                            // Doy formato de moneda
                            $debtAmountN = $this->moneyFormat($debtAmount);
                            $tDebtRateDebtPlusN = $this->moneyFormat($tDebtRateDebtPlus);
                            $return = "<tr>
                                                    <td style='text-align:center;font-size:13px;'>$quote</td>
                                                    <td style='text-align:center;font-size:13px;'>$expireComplete</td>
                                                    <td style='text-align:center;font-size:13px;'>$ceduNum</td>
                                                    <td style='text-align:center;font-size:13px;'>$debtAmountN</td>
                                                    <td style='text-align:center;font-size:13px;'>0,00</td>
                                                    <td style='text-align:center;font-size:13px;'>$charges</td>
                                                    <td style='text-align:center;font-size:13px;'>$fee</td>
                                                    <td style='text-align: right;font-size:13px;'>$tDebtRateDebtPlusN</td>
                                                </tr>";
                            echo $return;
                        }
                        // Si $total trae como valor "partial". Devuelvo datos parciales.
                        if ($total == "partial") {
                            // Realizo un pequeño hack para saber si $z es par o impar
                            // y así poder armar la tabla correcta y no mostrar resultados duplicados
                            ($z % 2 == 0) ? $return = "<tr><td style='text-align:center;font-size:9px;'>$quote</td><td style='text-align:center;font-size:9px;'>$ceduNum</td>" : $return = "<td style='text-align:center;font-size:9px;'>$quote</td><td style='text-align:center;font-size:9px;'>$ceduNum</td></tr>";
                            $z++;
                            echo $return;
                        }

                        // Sumo todos los totales de cada cuota
                        $debtTotal = $debtTotal + $tDebtRateDebtPlus;


                    }
                }


                // Devuelvo el total de los honorarios
                if ($total == "fee") {
                    return $feeTotal;
                }
                // Si $total contiene como parametro "total" devuelvo la suma de todo. Dependiendo el valor del $total, puedo calcular los valores de los diferentes vencimientos.
                if (strpos($total, 'total') !== false) {
                    // Trae los montos totales para el 2do y 3er vencimiento
                    // Array[0] = Monto 2do vto.
                    // Array[1] = Monto 3er vto.
                    if($whereFecha == 'si'){
                        if(!empty($fecha)){
                            $expDebts = explode("|", $this->rCalcRateDebtsForExpires('debts', $debtTotal, '', $fecha));
                        }
                    } else {

                        $expDebts = explode("|", $this->rCalcRateDebtsForExpires('debts', $debtTotal, '', false));
                    }
                    
                    $GLOBALS['tempDebtTotal'] = $this->simple_encrypt($debtTotal);

                    $debtTotal = $this->moneyFormat(bcdiv($debtTotal, 1, 2));
                    $debtTotal2 = $this->moneyFormat(bcdiv($expDebts[0], 1, 2));
                    $debtTotal3 = $this->moneyFormat(bcdiv($expDebts[1], 1, 2));

                    

                    switch ($total):
                        case "total1": return $debtTotal;
                            break;
                        case "total2": return $debtTotal2;
                            break;
                        case "total3": return $debtTotal3;
                            break;
                    endswitch;
                }
                
            }

            public function get_municipio()
            {
                
                $query = $this->db->get('zmunicipio');
                return $query->result();
            }

            public function get_registros($id,$type)
            {

                $this->db->select('Identificacion as identi');
                $this->db->select('Bien as bien');
                $this->db->select('Responsable as resp');
                $this->db->select('Domicilio as place');

                $this->db->where('Identificacion', $id);
                $this->db->where('BIEN_numero', $type);

                $query = $this->db->get('zvistabienes');
                return $query->result();
            }

            public function get_registro_id($id)
            {

                $this->db->select('Identificacion as identi');
                $this->db->select('Bien as bien');
                $this->db->select('Titular as resp');
                $this->db->select('Domicilio as place');

                $this->db->where('Identificacion', $id);
                $query = $this->db->get('zvistabienes');
                return $query->result();
            }

            public function getNextReceiptNum($update = FALSE) {
                $recNum = '';
                $this->db->select('DEUD_numero as recNum');
                $query = $this->db->get('_ultimos');

                $aReg = $query->result();

                //print_r($aReg);
                //exit;

                $recNum = $aReg[0]->recNum;

                // Si update es true actualizo el registro 
                if ($update) {
                    $recNum = $aReg[0]->recNum + 1;
                    $this->db->set('DEUD_numero', $recNum);
                    $this->db->update('_ultimos');
                }

                return $recNum;
            }


            public function comCode($return) {

                $this->db->select('LUGA_identidad as id');
                $this->db->select('LUGA_nombre as name');
                $this->db->select('LUGA_numero as id_lugar');
                //$this->db->where('LUGA_identidad !='' ');
                if($return != 'html'){
                    $this->db->where('LUGA_identidad is NOT NULL', NULL, FALSE);
                }
                $query =  $this->db->get('zlugarescobro');
                $aRegistro = $query->result();

                // Empresa y codigo
                foreach ($aRegistro as $aReg) {
                    $cId = $aReg->id;
                    $cName = $aReg->name;

                    if($aReg->id_lugar == 0){
                        $nombreFront = $aReg->name;
                    }
                    switch ($return):
                            case "html": $comCode = "$nombreFront";
                            break;
                        default:
                            $comCode = doBarcode($cId, $cName);
                            break;
                    endswitch;
                    echo $comCode;
                }
                
            }

            public function get_deudas($id,$type)
            {

                $this->db->select("TITA_abrev as t_name");
                $this->db->select("ANPE_vencimiento as expire_complete");
                $this->db->select("ANPE_vencimiento as expire_complete_traditional");
                $this->db->select("DEUD_numero as c_number");
                $this->db->select("ANPE_numero as a_number");
                $this->db->select("SITU_nombre as state_name");
                $this->db->select("SITU_numero as situation");
                $this->db->select("PROC_extrajud as situation_rate");
                $this->db->select("YEAR(ANPE_vencimiento) as expire_year");
                $this->db->select("MONTH(ANPE_vencimiento) as expire_month");
                $this->db->select("DAY(ANPE_vencimiento) as expire_day");
                $this->db->select("DEUD_monto as debt_amount");

                $this->db->where("BIEN_bien", $id);
                $this->db->where("BIEN_numero", $type);
                $this->db->not_like('ANPE_nombre',"TOTAL AÑO");

                $this->db->order_by('ANPE_vencimiento', 'desc');

                $query = $this->db->get('zvistadeudas');
                return $query->result();

            }

            public function get_zvistaplanes($id)
            {

                $this->db->select("PLAN_numero as t_name");
                $this->db->select("PLAN_vencimiento as expire_complete");
                $this->db->select("PLAN_vencimiento as expire_complete_traditional");
                $this->db->select("PLAN_numero as c_number");
                $this->db->select("PLAN_cedulon as a_number");
                $this->db->select("PLAN_totalcuotas as t_cuotas");
                $this->db->select("PLAN_cuota as p_cuotas");
                $this->db->select("SITU_nombre as state_name");
                $this->db->select("SITU_numero as situation");
                $this->db->select("PROC_extrajud as situation_rate");
                $this->db->select("YEAR(PLAN_vencimiento) as expire_year");
                $this->db->select("MONTH(PLAN_vencimiento) as expire_month");
                $this->db->select("DAY(PLAN_vencimiento) as expire_day");
                $this->db->select("PLAN_monto as debt_amount");

                $this->db->where("BIEN_bien", $id);

                $this->db->order_by('PLAN_vencimiento', 'desc');

                $query = $this->db->get('zvistaplanes');
                return $query->result();

            }

            function get_interes($type, $expireYearMonth)
            {
                date_default_timezone_set('America/Argentina/Buenos_Aires');
                $this->db->select('FECH_interes as rate');
                $this->db->select('FECH_anomes as iexpire_year_month');
                $this->db->where("BIEN_numero", $type);
                $this->db->where('FECH_anomes >='.$expireYearMonth);
                $fechaActual = date('Ym');
                $this->db->where('FECH_anomes <='.$fechaActual);
                $this->db->order_by('FECH_anomes', 'desc');
                
                $query = $this->db->get('fechasinteres');

                if ($query->num_rows() > 0)
                {
                    return $query = $query->result_array();
                                       
                }
            }

            public function proName()
            {
                $id = $this->input->cookie('identi',false);

                $this->db->select('PROC_nombre as proc_name');

                $this->db->where('BIEN_bien', $id);

                $this->db->group_by('PROC_nombre');

                $query = $this->db->get('zvistadeudas');
                $result = $query->result();

                return $result[0]->proc_name; 

            }


            public function codigos_de_barras() {

                $this->db->select('*');
                $query = $this->db->get('zlugarescobro');
                return $query->result();
                
            }

            // Filtra si que entidad debe generarse el código
            public function doBarcode($comId, $comName) {
                //print_r($comName);
                $name = strtolower(trim($comName));
                if (strpos($name, 'banco') !== false) {
                    echo $this->genBancoCordobaCode($comId, $name);
                } else {
                    echo $this->genPfRpCode($comId, $name);
                }
            }

            // Funcion que contruye el HTML para el barcode [PDF]
            function constructHtmlBarcode($barcodeImage, $headerImage) {
                //print_r($barcodeImage);
                $headerImage = strtoupper($headerImage);
                $return = "<tr><td class='text-left' style='font-size: 10px;font-weight: bold;padding: 2px !important'>Codigo de barra para: $headerImage</td></tr><tr class='text-center'><td style='padding: 2px !important'><img src='$barcodeImage' style='width: 353px; height: 35px;'/></td></tr>";
                return $return;
            }


            // Genera el código para el banco de cordoba
            function genBancoCordobaCode($comId, $comName) {

                $type = $this->input->cookie('type',false);
                $daysPlus = $_COOKIE["daysPlus"];
                $debtsSel = str_replace(",", "", $this->simple_decrypt($GLOBALS['tempDebtTotal']));
                $receipt = $this->getNextReceiptNum(FALSE);

                // Calculo las 3 fechas de vencimiento
                $dateExp1 = date('Ymd', strtotime(date('Ymd') . " +$daysPlus day"));
                $dateExp2 = date('Ymd', strtotime($dateExp1 . " +10 day"));
                $dateExp3 = date('Ymd', strtotime($dateExp2 . " +10 day"));

                // Corto los dos primeros dígitos del año
                $dateExp1 = substr($dateExp1, 2);
                $dateExp2 = substr($dateExp2, 2);
                $dateExp3 = substr($dateExp3, 2);
                // ----------- Fin calculo fechas de vencimiento -----------
                // Traigo la diferencia de intereses entre el 1er y 2do vencimiento y entre el 2do y 3er vencimiento
                // Lo doy formato de 2 decimales y lo completo a 4 caracteres en caso que sea necesario
                $expiresRates = explode("|", $this->rCalcRateDebtsForExpires('','',''));
                $expireSE = $this->DecFormat(bcdiv($expiresRates[0], 1, 2), '3');
                $expireTE = $this->DecFormat(bcdiv($expiresRates[1], 1, 2), '3');

                // Armo el código temporal
                $TempCodigo = substr($comId, 0, 4);
                $TempCodigo = $TempCodigo . "03";
                $TempCodigo = $TempCodigo . $this->intFormat($receipt, '16');
                $TempCodigo = $TempCodigo . $dateExp1;
                $TempCodigo = $TempCodigo . $this->intFormat($debtsSel, '9');
                $TempCodigo = $TempCodigo . '10';
                $TempCodigo = $TempCodigo . $expireSE;
                $TempCodigo = $TempCodigo . '10';
                $TempCodigo = $TempCodigo . $expireTE;
                $TempCodigo = $TempCodigo . $this->bankCodePrime($TempCodigo);

                $gcode = $this->simple_encrypt("cod");
                $code = $this->simple_encrypt($TempCodigo);
                $image = base_url() . "gen_barcode.php?$gcode=$code";
                return $this->constructHtmlBarcode($image, $comName);
            }


            // Genera el codigo para Pf = Pago Fácil | Rp = RapiPago
            function genPfRpCode($comId, $comName) {

                $type = $this->input->cookie('type',false);
                $daysPlus = $_COOKIE["daysPlus"];
                $debtsSel = str_replace(",", "", $this->simple_decrypt($GLOBALS['tempDebtTotal']));
                $coin = '0';
                $receipt = $this->intFormat($this->getNextReceiptNum(FALSE), '14');

                // Traigo la diferencia de intereses entre el 1er y 2do vencimiento y entre el 2do y 3er vencimiento
                $expiresRates = explode("|", $this->rCalcRateDebtsForExpires('','',''));
                $yearExp = date('y');
                // ---------------------------------------------------------------------

                $debtsExp1 = $this->intFormat($debtsSel, 8);
                // Calculo el valor a agregar al importe original, para el 2do y 3er vencimiento
                $debtsExp2 = $this->intFormat(bcdiv(($debtsSel * $expiresRates[0]) / 100, 1, 2), '6');
                $debtsExp3 = $this->intFormat(bcdiv(($debtsSel * $expiresRates[1]) / 100, 1, 2), '6');
                // ---------------------------------------------------------------------
                // Calculo las 3 fechas de vencimiento
                $dateExp1 = date('Ymd', strtotime(date('Ymd') . " +$daysPlus day"));
                $dateExp2 = date('Ymd', strtotime($dateExp1 . " +10 day"));
                $dateExp3 = date('Ymd', strtotime($dateExp2 . " +10 day"));
                // ---------------------------------------------------------------------
                // Formateo fechas y calculo diferencia de días
                $dtExp1 = date_create(date('d-m-Y', strtotime('01-01-' . DATE('Y'))));
                $dtExp1b = date_create(date('d-m-Y', strtotime($dateExp1 . " +1 day")));

                $dtExp2 = date_create(date('d-m-Y', strtotime($dateExp1)));
                $dtExp2b = date_create(date('d-m-Y', strtotime($dateExp2 . " +1 day")));

                $dtExp3 = date_create(date('d-m-Y', strtotime($dateExp3)));

                $daysExp1 = $this->intFormat(date_diff($dtExp1, $dtExp1b)->format('%a'), '3');
                $daysExp2 = $this->intFormat(date_diff($dtExp2, $dtExp2b)->format('%a'), '2');
                $daysExp3 = $this->intFormat(date_diff($dtExp1b, $dtExp3)->format('%a'), '2');
                // ---------------------------------------------------------------------
                // ----------- Fin calculo fechas de vencimiento -----------
                // Armo el código temporal
                $TempCodigo = substr($comId, 0, 4);
                $TempCodigo = $TempCodigo . $debtsExp1;
                $TempCodigo = $TempCodigo . $yearExp;
                $TempCodigo = $TempCodigo . $daysExp1;
                $TempCodigo = $TempCodigo . $receipt;
                $TempCodigo = $TempCodigo . $coin;
                $TempCodigo = $TempCodigo . $debtsExp2;
                $TempCodigo = $TempCodigo . $daysExp2;
                $TempCodigo = $TempCodigo . $debtsExp3;
                $TempCodigo = $TempCodigo . $daysExp3;

                if (strlen($TempCodigo) === 48) {
                    $TempCodigo = $TempCodigo . $this->otherCodePrime($TempCodigo);
                    if (strlen($TempCodigo) === 49) {
                        $TempCodigo = $TempCodigo . $this->otherCodePrime($TempCodigo);
                    }
                }
                $gcode = $this->simple_encrypt("cod");
                $code = $this->simple_encrypt($TempCodigo);

                $image = base_url() . "gen_barcode.php?$gcode=$code";
                return $this->constructHtmlBarcode($image, $comName);
            }


            // Formato para enteros [BARCODE]
            function intFormat($value, $lenght) {
                // Quito el punto del valor
                $value = str_replace(".", "", $value);
                // Agrego ceros por si en necesario
                $return = substr("0000000000000000000000000" . $value, -$lenght);
                return $return;
            }

            // Formato primos para otras entidades [BARCODE]
            function otherCodePrime($code) {
                $pNum = 9;
                // Multiplico la serie por los números primos 9 7 1 3
                for ($i = 0; $i < strlen($code); $i++) {
                    $return = $return + (substr($code, $i, 1) * $pNum);

                    ($pNum === 9) ? $pNum = 3 : $pNum = $pNum + 2;
                }
                $return = (intval($return) / 2);
                $return = ($return / 10);
                $return = round(($return - intval($return)), 2);
                $return = $return * 10;
                return intval($return);
            }

            function simple_encrypt($text) {
                return trim(base64_encode($text));
            }

            function simple_decrypt($text) {
                return trim(base64_decode($text));
            }

            // Formato primos para bancos [BARCODE]
            function bankCodePrime($code) {
                $pNum = 9;
                // Multiplico la serie por los números primos 9 7 1 3
                for ($i = 0; $i < strlen($code); $i++) {
                    $return = $return + (substr($code, $i, 1) * $pNum);

                    switch ($pNum):
                        case $pNum === 9: $pNum = 7;
                            break;
                        case $pNum === 7: $pNum = 1;
                            break;
                        case $pNum === 1: $pNum = 3;
                            break;
                        case $pNum === 3: $pNum = 9;
                            break;
                    endswitch;
                }

                // Obtengo la unidad del número y le resto 10
                if (strlen($return) > 2) {
                    $return = 10 - (substr($return, 2, 1));
                } else {
                    $return = 10 - (substr($return, 1, 1));
                }
                ($return == 10) ? $return = 0 : "";

            // Retorno el resultado
                return $return;
            }


            // Formato para decimales [BARCODE]
            function DecFormat($value, $lenght) {
                // Quito el punto del valor
                $value = str_replace(".", "", $value);
                $return = $value;
                // Si el largo es igual o menos al parámetro lenght, le agrego un 0 adelante
                if (strlen($value) <= $lenght) {
                    $return = "0" . $value;
                }
                // Retorno el resultado
                return $return;
            }


            public function receiptInsert($rNum, $exp1, $exp2, $exp3, $dbt1, $dbt2, $dbt3) {
                // Conexion a la DB
                
                // Vuelvo a dar formato a las fechas para que respeten los de la base
                $exp1 = DateTime::createFromFormat('d/m/Y', $exp1)->format('Y-m-d');
                $exp2 = DateTime::createFromFormat('d/m/Y', $exp2)->format('Y-m-d');
                $exp3 = DateTime::createFromFormat('d/m/Y', $exp3)->format('Y-m-d');

                // Quito los puntos y luego reemplazo las comas por puntos
                $dbt1 = str_replace(",", ".", str_replace(".", "", $dbt1));
                $dbt2 = str_replace(",", ".", str_replace(".", "", $dbt2));
                $dbt3 = str_replace(",", ".", str_replace(".", "", $dbt3));

                // Obtengo los intereses para los 3 vencimientos
                $rates = explode("|", $this->rCalcRateDebtsForExpires("rate", 0, 3));
                $rate1 = $rates[0];
                $rate2 = $rates[1];
                $rate3 = $rates[2];

                // Obtengo el total de los honorarios
                $fee = $this->get_table_generator("fee");

                // Obtengo el tipo de bien
                $bNum = $this->input->cookie('type',false);
                // Obtengo el id del bien
                $id = $this->input->cookie('identi',false);

                // Creo el INSERT para recibos y lo ejecuto

                $data = array(
                    'RECI_numero' => $rNum,
                    'RECI_vencimiento1' => $exp1,
                    'RECI_vencimiento2' => $exp2,
                    'RECI_vencimiento3' => $exp3,
                    'RECI_monto1' => $dbt1,
                    'RECI_monto2' => $dbt2,
                    'RECI_monto3' => $dbt3,
                    'BIEN_numero' => $bNum,
                    'BIEN_bien' => $id,
                    'RECI_interes1' => $rate1,
                    'RECI_interes2' => $rate2,
                    'RECI_interes3' => $rate3,
                    'RECI_honorarios1' => $fee,
                    'RECI_honorarios2' => $fee,
                    'RECI_honorarios3' => $fee,
                    'USER_fecha' => date("Y-m-d H:i:s"),
                    'USER_numero' => 999,
                );

                $this->db->insert('recibos', $data);

                // Creo el UPDATE para _ultimos y lo ejecuto
                $this->db->set('DEUD_numero', $rNum);
                $this->db->update('_ultimos');
            }


    }
    
    ?>
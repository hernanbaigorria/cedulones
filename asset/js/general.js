// Función general para cerrar el modal
function closeBootBox(miliseconds) {
    window.setTimeout(function () {
        bootbox.hideAll();
    }, miliseconds);
}

$( function() {
    var f1 = new Date();
    var fechaActual = f1.getDate() + "/"+ (f1.getMonth() + 1)+ "/" +f1.getFullYear();
   // setCookie("seleccionDia", fechaActual);
   // alert(getCookie("seleccionDia"));
    if (getCookie("seleccionDia") === undefined) {
        var diaSeleccionado = fechaActual;
    } else {
        var diaSeleccionado = getCookie("seleccionDia");
    }

    $( "#datepicker" ).datepicker({
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        duration: "fast",
        minDate: 0,
        maxDate: "30D",
    }
    );

    $("#datepicker").datepicker().datepicker("setDate", diaSeleccionado);

} );

// Flecha que vuelve atrás
$(".icon-fas-arrow-back").click(function () {
    window.history.back();
});

// Wrap every letter in a span
$('.ml12').each(function () {
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
});

anime.timeline({loop: true})
        .add({
            targets: '.ml12 .letter',
            translateX: [40, 0],
            translateZ: 0,
            opacity: [0, 1],
            easing: "easeOutExpo",
            duration: 1200,
            delay: function (el, i) {
                return 500 + 30 * i;
            }
        }).add({
    targets: '.ml12 .letter',
    translateX: [0, -30],
    opacity: [1, 0],
    easing: "easeInExpo",
    duration: 1100,
    delay: function (el, i) {
        return 100 + 30 * i;
    }
});

// Cuando se termina de cargar el DOM
$(document).ready(function () {
    // Función temporal para remover la marca de agua del hosting gratuito
    $('html div[style*="text-align: right"]').remove();

    // Validación pura en JS.
    // Se ocupa de mostrar el footer con el importe seleccionado solo en la sección que le corresponde
    if (/detalle_deudas/.test(window.location.href)) {
        $('.table_pay_bottom_fixed').removeClass("hidden");
        $('.table_pay_bottom_fixed_lg').removeClass("hidden");
    }

    // Cookie que utilizo para contar la cantidad de registros seleccionados
    setCookie("countSelected", 0);

    // Cuando se abre un modal
    $(document).on('shown.bs.modal', function () {
        // autofocus todos el input que tiene seteada la opcion 'autofocus' una vez que se carga el dialog
        $(this).find("[autofocus]").focus();
    });

    // Funcion para cambiar el cursor en todas las precargas de ajax
    function globalAjaxCursorChange()
    {
        $(document).ajaxStart(function () {
            $("html").addClass('ajaxLoading');
        });
        $(document).ajaxStop(function () {
            $("html").removeClass('ajaxLoading');
        });
    }
    setInterval(globalAjaxCursorChange, 1000);

    // Igualo heights 
});

function base64EncDec(string, wdo) {
    var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
            var t = "";
            var n, r, i, s, o, u, a;
            var f = 0;
            e = Base64._utf8_encode(e);
            while (f < e.length) {
                n = e.charCodeAt(f++);
                r = e.charCodeAt(f++);
                i = e.charCodeAt(f++);
                s = n >> 2;
                o = (n & 3) << 4 | r >> 4;
                u = (r & 15) << 2 | i >> 6;
                a = i & 63;
                if (isNaN(r)) {
                    u = a = 64
                } else if (isNaN(i)) {
                    a = 64
                }
                t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
            }
            return t
        }, decode: function (e) {
            var t = "";
            var n, r, i;
            var s, o, u, a;
            var f = 0;
            e = e.replace(/\++[++^A-Za-z0-9+/=]/g, "");
            while (f < e.length) {
                s = this._keyStr.indexOf(e.charAt(f++));
                o = this._keyStr.indexOf(e.charAt(f++));
                u = this._keyStr.indexOf(e.charAt(f++));
                a = this._keyStr.indexOf(e.charAt(f++));
                n = s << 2 | o >> 4;
                r = (o & 15) << 4 | u >> 2;
                i = (u & 3) << 6 | a;
                t = t + String.fromCharCode(n);
                if (u != 64) {
                    t = t + String.fromCharCode(r)
                }
                if (a != 64) {
                    t = t + String.fromCharCode(i)
                }
            }
            t = Base64._utf8_decode(t);
            return t
        }, _utf8_encode: function (e) {
            e = e.replace(/\r\n/g, "n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r)
                } else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128)
                } else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128)
                }
            }
            return t
        }, _utf8_decode: function (e) {
            var t = "";
            var n = 0;
            var r = c1 = c2 = 0;
            while (n < e.length) {
                r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                    n++
                } else if (r > 191 && r < 224) {
                    c2 = e.charCodeAt(n + 1);
                    t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                    n += 2
                } else {
                    c2 = e.charCodeAt(n + 1);
                    c3 = e.charCodeAt(n + 2);
                    t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    n += 3
                }
            }
            return t
        }}
    if (wdo === 'dec') {
        rString = Base64.decode(string);
    } else {
        rString = Base64.encode(string);
    }
    return rString;
}

function setCookie(key, value) {
    var defaults = {expires: 1, path: '/'};
    Cookies.remove(key);
    Cookies.set(key, value, defaults);
}

function getCookie(key) {
    return Cookies.get(key);
}

// Cuando la resolución cambia
$(window).bind('resize', function () {
    equalToHeights('tabpay1', 'tabpay2');
});

restaFechas = function(f1,f2)
 {
 var aFecha1 = f1.split('/'); 
 var aFecha2 = f2.split('/'); 
 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
 var dif = fFecha2 - fFecha1;
 var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
 return dias;
 }

$(".selDaysPlus").change(function () {
    setCookie("seleccionDia", $(this).val());
    var f1 = new Date();
    var fechaActual = f1.getDate() + "/"+ (f1.getMonth() + 1)+ "/" +f1.getFullYear();
    var f2= $(this).val();
    var cantidadDeDias = restaFechas(fechaActual,f2);
    setCookie("seleccionDia", $(this).val());

    setCookie("daysPlus", cantidadDeDias);

    $("body").addClass("m-page--loading");
    window.location.reload();
});

function rCalcDebtsAndExpires(type) {
    debtsSelecteds = $("#inputDetailTotal").val();
    $.ajax({
        type: "POST",
        cache: true,
        async: true,
        url: "/testq.php",
        data: "action=rCalcDebtsAndExpires" + "&debtsSel=" + debtsSelecteds + "&type=" + type,
        success: function (rsp) {
            console.log(rsp);
        }
    });
}

// Función que se ejecuta cuando se clickea sobre inmuebles, rodados, comercio o cementerio
$(".bootid").click(function () {
    var isType = $(this).attr("istype");
    idNum(isType);
});

// Función encargada de limpiar todo los checks, atributos relacionados y reiniciar los calculos
// De momento no se utiliza ésta función. 
function clearAllPayChecks() {
    $("input.checkboxCalc:checkbox").prop('checked', false).removeAttr('checked');
    $(".payDetail").prop('checked', false).removeAttr('checked');
    $(".payDetail").attr("all-on", '0');
    $("#inputDetailTotal").val("0.00");
    $(".detailTotal").html("0.00");
}

// Cuando se hace click en el boton que se seleccionan todos
$(".payDetail").click(function () {
    // El suffix informa al código sobre cuales checkboxes tiene que accionar
    // Ésto se diferencia ya que hay dos checkboxes diferentes
    // Uno de ellos para mobile y otro para pantallas grandes
    checkboxSuffix = $(this).attr("check-box-suffix");
    // Declaro por separado los que estan check y los que no para luego 
    // poder hacer bien la cuenta del total en caso de que se seleccione o deseleccione uno
    checkDetailNot = $("input[name='checkbox" + checkboxSuffix + "']:not(:checked)");
    checkDetailIs = $("input[name='checkbox" + checkboxSuffix + "']:checked:enabled");

    // all-on es un attributo del check prinicpal (el que selecciona todos)
    // Se utiliza para saber si al hacer click se está checkeando o descheckeando todos los checks disponibles
    // 0 = Ninguno checkeado || 1 = Todos checkeados
    if ($(this).attr("all-on") === '0') {
        // Por cada uno que NO está checkeado, lo checkeo y ejecuto la función que se encarga del cáculo. (En éste caso va a ser suma)
        checkDetailNot.each(function () {
            $(this).prop('checked', true).attr('checked', 'checked');
            calcTotalPayDetail($(this));
        });
        // Seteo all-on en 1 porque ahora se checkearon todos los disponibles
        $(this).attr("all-on", '1');
    } else {
        // Por cada uno que SI está checkeado, lo descheckeo y ejecuto la función que se encarga del cáculo. (En éste caso va a ser resta)
        checkDetailIs.each(function () {
            $(this).prop('checked', false).removeAttr('checked');
            calcTotalPayDetail($(this));
        });
        // Seteo all-on en 0 porque ahora se descheckearon todos los disponibles
        $(this).attr("all-on", '0');
    }
});

// Si hago click en algún check de manera independiente, también ejecuto la función que se encarga del cálculo
$("input.checkboxCalc:checkbox").click(function () {
    calcTotalPayDetail($(this));
});

// Función que se encarga del cálculo
function calcTotalPayDetail(checkClicked) {
    // regNumber es un numero que identifica al check. Éste número también está en el dato de la tabla que contiene el importe
    // Esto se hace para poder obtener cada importe diferenciado.
    regNumber = $(checkClicked).attr('reg-number');

    // debAmount contiene cada importe diferenciado para luego realizar la suma o resta según sea el caso
    debtAmount = parseFloat($("#debtCalc" + regNumber).html().replace("$", ""));
    // cNumber contiene cada numero de cedulon diferenciado
    // inputCNumberParcial = Variable que se utiliza para colocar los valores de manera independientes
    cNumber = $("#cNumber" + regNumber).html();
    inputCNumberParcial = $("#cNumbers").val();

    // Datos de cada cuota para paypertic
    cuoteForPPT = $("#cuote" + regNumber).attr("cuote");
    debtForPPT = $("#cuote" + regNumber).attr("debteach");
    dataParcialForPPT = cuoteForPPT + "_" + debtForPPT + "-";

    // inputParcial = Es una variable que se utiliza para colocar el resultado parcial que se encuentra
    // en un campo que no está visible
    // inputEachParcial = Variable que se utiliza para colocar los valores de manera independientes
    inputParcial = parseFloat($("#inputDetailTotal").val());
    inputEachParcial = $("#inputDetailEach").val();

    // Input donde almaceno los datos para ppt
    inputForPPT = $("#inputForPPT").val();


    // Valida si el check está seleccionado
    // En caso que sea cierto, se procede a sumar el monto parcial del campo con el valor diferenciado obtenido de la tabla
    // Si el check no está seleccionado, se procede a restar
    // toFixed redondea a dos decimales
    // inputEachFinal concatena los valores diferenciados, divididos entre si con guiones para poder luego hacer un explode
    // Esto es un pequeño hack para poder luego mostrarlo en el PDF que se generará
    // A la vez manejo un contador de checks seleccionados [cs] y lo almaceno en una cookie
    // Controlo los datos a pasar para ppt
    if ($(checkClicked).is(":checked")) {
        inputFinal = (inputParcial + debtAmount).toFixed(2);
        inputEachFinal = inputEachParcial + "-" + debtAmount;
        inputCNumberParcial = inputCNumberParcial + cNumber + "-";
        dataForPPT = inputForPPT + dataParcialForPPT;
        // ---- Contador de registros seleccionados ----
        cs = parseInt(getCookie("countSelected")) + 1;
        setCookie("countSelected", cs);
        // -- Fin contador de registros seleccionados --
    } else {
        inputFinal = (inputParcial - debtAmount).toFixed(2);
        inputEachFinal = inputEachFinal.replace("-" + debtAmount, "");
        inputCNumberParcial = inputCNumberParcial.replace(cNumber + "-", "");
        dataForPPT = inputForPPT.replace(dataParcialForPPT, "");
        // ---- Contador de registros seleccionados ----
        cs = parseInt(getCookie("countSelected")) - 1;
        setCookie("countSelected", cs);
        // -- Fin contador de registros seleccionados --
    }

    // inputDetailTotal es campo que no está visible pero se utiliza para colocar el resultado final
    // y luego utilizarlo para nutrirse de él y poder mostrarlo en el html
    // inputDetailEach es campo que no está visible pero se utiliza para colocar de manera diferenciada 
    // todos los valores seleccionados y luego utilizarlo para nutrirse de él y poder generar el PDF
    $("#inputDetailTotal").val(inputFinal).trigger("change");
    $("#inputDetailEach").val(inputEachFinal);
    $("#cNumbers").val(inputCNumberParcial);

    // Input donde almaceno los datos para ppt
    $("#inputForPPT").val(dataForPPT);
}

// Valida si el valor del campo oculto cambia
// Si esto sucede y su valor es mayor a cero, se habilitan los botones de impresión y pago
// Caso contrario, estos se deshabilitan
// Por otra parte, sin condicional, siempre que el campo oculto se modifique, se proyecta su valor en el htm
$("#inputDetailTotal").on("change paste keyup select", function () {
    if ($("#inputDetailTotal").val() > 0) {
        $(".btn-cedu").removeClass("disabled");
        $(".btn-pay-on").removeClass("disabled");
    } else {
        $(".btn-cedu").addClass("disabled");
        $(".btn-pay-on").addClass("disabled");
    }
    $(".detailTotal").html($("#inputDetailTotal").val());
});

// Botones de imprimir cedulón y pago online
$(".btn-cedu").click(function () {
    // Si el boton está disabilitado no hago nada
    if ($(this).hasClass("disabled") !== false) {
        return false;
    }

    var post_url = $('.tablaDeudas').attr("action"); //get form action url

    var request_method = $('.tablaDeudas').attr("method"); //get form GET/POST method

    //var numdeuda = $('input[name="checkbox3[]"]:checked').attr("val-table"); //Encode form elements for submission

    var checkboxes = new Array();
    $("input:checked").each(function() {
       checkboxes.push($(this).val());
    });
    setCookie('checkbox', checkboxes);
    var numdeuda = new Array();
    $("input:checked").each(function() {
       numdeuda.push($(this).attr("val-table"));
    });
    setCookie('numdeuda', numdeuda);


    $.ajax({
        url : post_url,
        type: request_method,
        data : {checkbox:checkboxes, numdeuda:numdeuda, accion:'pdf'},
        beforeSend: function() {
           
        },
        success: function(response){
            var res = $.parseJSON(response);
            $('<a href='+res.url+' download></a>')[0].click();
            var file = res.nombre;
            $.ajax({
                url : '../borrar/',
                type: 'post',
                data : {file:file}
            });
        }
    });

});

// Botones de imprimir cedulón y pago online
$(".btn-pay-on").click(function () {
    // Si el boton está disabilitado no hago nada
    if ($(this).hasClass("disabled") !== false) {
        return false;
    }
    setCookie(base64EncDec('cnumbs'), base64EncDec($("#cNumbers").val()));
    // Paso los datos para generar en ppt
    var data = $("#inputForPPT").val();
    // Muestro el loader por que la operación demora
    //$(".ml12").removeClass("hidden");
    //$('body').addClass('m-page--loading');

    $.ajax({
        type: "POST",
        cache: true,
        async: true,
        url: "../pago/",
        data: "data=" + data,
        success: function (rsp) {

            alert(rsp);
            // Quito el loader
            //$('body').removeClass('m-page--loading');
            //$(".ml12").addClass("hidden");
            //window.open(rsp, '_blank');
        }
    });
});

function mulPagesPDF(param) {
    $.ajax({
        type: "POST",
        cache: true,
        async: true,
        url: "./scripts/pdf_gen.php",
        data: "data=" + param,
        success: function (rsp) {
            console.log(rsp);
        }
    });

}

// Función encargada de darle formato a un campo.
// Temporalmente solo aplica para inmuebles
function formatInput(inputId) {
//    console.log(inputId);
    switch (inputId) {
        case "inputInm":
            format = [2, 2, 3, 3, 3];
            var cleave = new Cleave('#' + inputId, {
                delimiter: '-',
                blocks: format,
                uppercase: true
            });
            break;
    }
}


function idNum(type) {
    // Ésta primer parte de la función (switch) se encarga de setear datos específicos para el modal para cada categoría
    placeHoler = '';
    isType = 'text';
    switch (type) {
        case "inm":
            title = "inmuebles";
            friUrl = title;
            inputFill = 'fas fa-home';
            inputId = 'inputInm';
            placeHoler = '__-__-___-___-___';
            type_name = 'tipo_inmueble';
            type_value= 1;
            break;
        case "rod":
            title = "rodados";
            friUrl = title;
            inputFill = 'fas fa-car';
            inputId = 'inputRod';
            type_name = 'tipo_rodados';
            type_value= 2;
            break;
        case "com":
            title = "Comercio e Industria";
            friUrl = "comercio_industria";
            inputFill = 'fas fa-industry';
            inputId = 'inputCom';
            type_name = 'tipo_comercio';
            type_value= 3;
            break;
        case "cem":
            title = "cementerios";
            friUrl = title;
            inputFill = 'fas fa-cross';
            inputId = 'inputCem';
            type_name = 'tipo_cementerios';
            type_value= 4;
            break;
    }

    // Función que arma el modal con los datos definidos arriba
    // Mediante un AJAX envía la información y valida si lo que se busca
    // se encuentra en la base de datos
    // Si encuentra un valor, lleva a la página correspondiente de cada categoría
    var fill = '<div class="input-group"><div class="input-group-addon"><i class="' + inputFill + '"></i></div>';
    fill += '<input type="' + isType + '" pattern="^[0-9/ -]+$" autocomplete="off" autofocus name="' + inputId + '" id="' + inputId + '"class="form-control input-bootbox" placeholder="' + placeHoler + '" >';
    fill += '<input type="hidden" name='+type_name+' value='+type_value+'>';
    fill += '</div>';
    bootbox.dialog({
        title: title,
        message: fill,
        buttons: {
            // Estilos y funciones del boton BUSCAR
            'confirm': {
                label: 'Buscar',
                className: 'btn btn btn-primary',
                // Con result valida si se hizo click en el boton
                callback: function (result) {
                    if (result) {
                        // Valida que el campo donde se carga el identificador no esté vacio
                        if ($('#' + inputId).val() !== "") {
                            setCookie(base64EncDec("identification"), base64EncDec($('#' + inputId).val()));
                            setCookie(base64EncDec("type"), base64EncDec(inputId));
                            var identi = $('input[name="' + inputId + '"]').val();
                            var type = $('input[name="' + type_name + '"]').val();
                            $.ajax({
                                type: "POST",
                                cache: true,
                                async: true,
                                url: 'rdatacheck',
                                // Envía mediante AJAX el valor del campo (identificador) y el tipo de categoría
                                data: {identi: identi , type: type},
                                // Primero valida si el dato se encuentra en la base
                                success: function (rsp) {
                                    // Si no se encuentra, crea un modal con una alerta notificando
                                    if (rsp === 'NB' || rsp === 'ND') {
                                        if (rsp === 'NB') {
                                            // No se encontró ningun bien
                                            mess = "<h5 class='text-center'>No se encontró ningún registro relacionado</h5>";
                                        } else if (rsp === 'ND') {
                                            // No se encontró ninguna deuda para el bien
                                            mess = "<h5 class='text-center'>No posee ninguna deuda</h5>";
                                        }
                                        bootbox.alert({
                                            title: "¡Atención!",
                                            message: mess,
                                            backdrop: true,
                                            callback: function () {
                                                // El timeout solo está para que el foco vuelva correctamente al campo cuando se cierra el modal de alerta
                                                setTimeout(function () {
                                                    $('input[name="' + inputId + '"]').focus();
                                                    $('input[name="' + inputId + '"]').removeClass("onFocusError");
                                                }, 500);

                                            }
                                        });
                                    } else {
                                        // Si se encuentra el dato, se redirige a la página encargada de cargar todos los datos correspondientes
                                        window.location = "detalle/?type="+friUrl;
                                    }
                                }
                            });
                        } else {
                            // Si el campo está vacio, le hace foco y le agrega una clase (CSS) de alerta
                            $('input[name="' + inputId + '"]').focus();
                            $('input[name="' + inputId + '"]').addClass("onFocusError");
                            return false;
                        }
                    }
                    return false;
                }
            },
            // Estilos y funciones del boton BUSCAR
            // Tiene un callback pero en éste caso no lo usamos
            'cancel': {
                label: 'Cancelar',
                className: 'btn btn btn-danger pull-right custom-margin-left-15px', callback: function () {
//                    console.log("just do something on close");
                }
            }
        }
    });
    // Habilito a ejecutar la búsqueda del modal con el ENTER también
    // Lo pongo al final para poder tomar el objeto que se crea recién cuando se abre el modal.
    $('.input-bootbox').on('keyup', function (e) {
        if ((e.keyCode || e.which) === 13) {
            $('.btn-primary').trigger('click');
        }
    });
    // Luego que genero todo el modal, llamo a la función que da formato al campo
    // donde se ingresa el identificador. Le paso como dato la categoría 
    formatInput(inputId);
}